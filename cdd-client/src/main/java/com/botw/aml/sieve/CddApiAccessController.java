package com.botw.aml.sieve;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

/**
 * Created by pandyc01 on 03/22/2016.
 */


@RestController
//@ResponseStatus(HttpStatus.OK)
public class CddApiAccessController {

    @Autowired
   DiscoveryClient client;

  //  @Autowired
//    FeignClientController feignClientController;

   @RequestMapping("/info")
    public @ResponseBody
   String getSentence() {
        return String.format("%s %s %s",
                getWord("CDD-INFOSERVICE"),
                getWord("MICROSERVICE2-SERVICE"),
                getWord("CDD-EVAL"));

    }

    public String getWord(String service) {
        List<ServiceInstance> list = client.getInstances(service);
        if (list != null && list.size() > 0 ) {
            URI uri = list.get(0).getUri();
            if (uri !=null ) {
                System.out.println("URI ::" + uri.toString());
                //return uri.toString();
                return (new RestTemplate()).getForObject(uri+"/info",String.class);
            }
        }
        return null;
    }



}