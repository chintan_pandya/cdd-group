package com.botw.aml.sieve.eval;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CddEvalApplication {

	public static void main(String[] args) {
		SpringApplication.run(CddEvalApplication.class, args);
	}
}
