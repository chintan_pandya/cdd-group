package com.botw.aml.sieve.eval.application;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ApplicationServiceRegistry implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    public static EvaluationApplicationService evaluationApplicationService() {
        return (EvaluationApplicationService) applicationContext.getBean("evaluationApplicationService");
    }


    @Override
    public synchronized void setApplicationContext(
            ApplicationContext anApplicationContext)
            throws BeansException {

        if (ApplicationServiceRegistry.applicationContext == null) {
            ApplicationServiceRegistry.applicationContext = anApplicationContext;
        }
    }
}