package com.botw.aml.sieve.eval.application;

import com.botw.aml.sieve.eval.application.commands.EvaluateCommand;
import com.botw.aml.sieve.eval.application.commands.InitiateEvaluationCommand;
import com.botw.aml.sieve.eval.domain.model.Evaluation;
import com.botw.aml.sieve.eval.domain.model.EvaluationService;
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResultRepository;
import com.botw.aml.sieve.eval.domain.model.RiskModel;
import com.botw.aml.sieve.eval.domain.model.RiskModelFactory;
import com.botw.aml.sieve.eval.domain.model.admin.Answer;
import com.botw.aml.sieve.eval.domain.model.admin.Question;
import com.botw.cdd.model.riskModel.CddProvidedAnswer;
import com.botw.cdd.model.riskModel.CddQuestion;
import com.botw.cdd.model.riskModel.CddPossibleAnswer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by krasnd52 on 12/8/15.
 */
@Transactional
@Service
public class EvaluationApplicationService {
    @Autowired
    private KycEvaluationResultRepository kycEvaluationResultRepository;

    @Autowired
    private RiskModelFactory riskModelFactory;


    private RiskModel riskModel;

    public EvaluationApplicationService()
    {

    }

    public RiskModel getRiskModel()
    {
        if(this.riskModel == null){
            this.riskModel = riskModelFactory.createRiskModel();
        }
        return this.riskModel;
    }

    @Autowired
    private EvaluationService evaluationService;

    private EvaluationService evaluationService() {
        return evaluationService;
    }

    /**
     * Schedules an evaluation of a KYC profile by the Sieve engine. This method is non-blocking and returns immediately
     * (i.e. before the evaluation has finished). The returned Evaluation object allows you to query
     * the progress of the transfer, add listeners for progress events, and wait for the upload to complete.
     * <p/>
     * If resources are available, the upload will begin immediately, otherwise it will be scheduled and started as soon
     * as resources become available.
     *
     * @param aCommand
     *
     * @return A new Evaluation object which can be used to check state of the evaluation, listen for progress
     * notifications, and otherwise manage the upload.
     */
    public Evaluation initiateEvaluation(InitiateEvaluationCommand aCommand) {
        Evaluation evaluation =
                this.evaluationService()
                        .submit(aCommand.getKycProfile().getPartyContext().getAmlPartyId(),
                                aCommand.getKycProfile().getPartyContext().getPartyKeys(),
                                aCommand.getKycProfile().getKycFactIds(),
                                aCommand.getKycProfile().getKyaProfiles(),
                                this.getRiskModel());
        return evaluation;
    }

    /**
     * The synchronous method for evaluating a KYC profile.
     *
     * @param aCommand
     */
    public void evaluate(EvaluateCommand aCommand) {

        KycEvaluationResult evaluationResult =
                this.evaluationService().evaluate(
                        aCommand.getEvaluationId(),
                        aCommand.getInitiateEvaluationCommand().getKycProfile().getPartyContext().getAmlPartyId(),
                        aCommand.getInitiateEvaluationCommand().getKycProfile().getPartyContext().getPartyKeys(),
                        aCommand.getInitiateEvaluationCommand().getKycProfile().getKycFactIds(),
                        aCommand.getInitiateEvaluationCommand().getKycProfile().getKyaProfiles(),
                        this.getRiskModel());
        System.out.println("Evaluation result"+evaluationResult);
        this.kycEvaluationResultRepository.save(evaluationResult);

    }

        public List<CddQuestion> nextQuestions(CddProvidedAnswer cddProvidedAnswer) {

        List<Question> nextQuestions = this.getRiskModel().getNextQuestions(
                        cddProvidedAnswer.getQuestionCode(),
                        cddProvidedAnswer.getProvidedAnswerCodes());

        List<CddQuestion> nextCddQuestions = new ArrayList<CddQuestion>();

        if(null!=nextQuestions) {
            for (Question question : nextQuestions) {
                List<CddPossibleAnswer> possibleAnswers = new ArrayList<CddPossibleAnswer>();

                if(null!=question.getAnswerFormatType() && "Free Text"!=question.getAnswerFormatType()) {
                    for (Answer answer : question.getPossibleAnswersWithRiskFactorValues()) {
                        CddPossibleAnswer cddPossibleAnswer = new CddPossibleAnswer(
                                answer.getAnswerCode(),
                                answer.getAnswerText()
                        );
                        possibleAnswers.add(cddPossibleAnswer);
                    }
                }

                CddPossibleAnswer defaultAnswer = null;
                if (question.getDefaultAnswer() != null) {
                    defaultAnswer = new CddPossibleAnswer(
                            question.getDefaultAnswer().getAnswerCode(),
                            question.getDefaultAnswer().getAnswerText());
                }


                CddQuestion cddQuestion = new CddQuestion(
                        question.getQuestionCode(),
                        question.getAnswerFormatType(),
                        question.getQuestionText(),
                        possibleAnswers,
                        defaultAnswer

                );

                nextCddQuestions.add(cddQuestion);
            }
        }
        
        return nextCddQuestions;
    }


    @Transactional(readOnly = true)
    public KycEvaluationResult evaluationResultOf(String anEvaluationId) {
     KycEvaluationResult evaluationResult =
                this.kycEvaluationResultRepository
                        .findOne(anEvaluationId);

        return evaluationResult;
    }


    @Transactional(readOnly = true)
    public List<KycEvaluationResult> evaluationResultOfParty(String partyId) {
    return this.kycEvaluationResultRepository.findByAmlPartyId(partyId);
    }

    private KycEvaluationResultRepository KycEvaluationResultRepository() {
        return this.kycEvaluationResultRepository;
    }

    public Set<String>getQuestionCode(Set<String> riskFacts,boolean isSkipped){
        return this.riskModel.convertRiskFactorCodeToQuestionCode(riskFacts,isSkipped);
    }

}
