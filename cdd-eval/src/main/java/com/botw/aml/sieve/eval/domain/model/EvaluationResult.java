package com.botw.aml.sieve.eval.domain.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by krasnd52 on 12/4/15.
 * <p/>
 * Encapsulate an evaluation result of a set of facts. May be used for KYA, KYC, or, if preferred, its
 * use may be slightly refactored so it can be used for a subtree (i.e. what the business calls a 'risk factor' today
 */
public final class EvaluationResult implements Serializable {
    private final Set<String> usedInput;
    private final Set<String> skippedInput;
    private final Set<String> missingRiskFactors;
    private double score;


    public EvaluationResult(Set<String> factIds) {
        this.score = 0;
        this.skippedInput = new HashSet<String>(factIds);
        this.usedInput = new HashSet<String>();
        this.missingRiskFactors = new HashSet<String>();
    }

    public Set<String> getUsedInput() {
        return usedInput;
    }

    public Set<String> getSkippedInput() {
        return skippedInput;
    }

    public Set<String> getMissingRiskFactors() {
        return missingRiskFactors;
    }

    public double getScore() {
        return score;
    }

    public void increaseScore(double score) {
        this.score += score;
    }

    public void addMissingRiskFactor(String aMissingRiskFactorId) {
        this.missingRiskFactors.add(aMissingRiskFactorId);
    }

    public void addFoundFact(String aFoundFactId) {
        this.usedInput.add(aFoundFactId);
    }

    public void removeFromSkippedInput(String aFoundFactId) {
        this.skippedInput.remove(aFoundFactId);
    }

}
