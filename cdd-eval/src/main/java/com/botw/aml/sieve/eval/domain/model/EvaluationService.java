package com.botw.aml.sieve.eval.domain.model;

import com.botw.aml.sieve.eval.domain.model.party.*;
import com.botw.aml.sieve.eval.domain.model.account.KyaProfile;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

/**
 * Created by krasnd52 on 12/8/15.
 */
@Service
public class EvaluationService {



    public Evaluation submit(final AmlPartyId anAmlPartyId,
                             final Set<PartyKey> partyKeys,
                             final Set<String> kycFactIds,
                             final Collection<KyaProfile> kyaProfiles,
                             RiskModel riskModel) {
        return new SpringScheduledEvaluation(
                EvaluationId.newEvaluationId(),
                new EvaluationProgress(),
                new KycProfile(
                        PartyContext.createPartyContext(
                                anAmlPartyId,
                                partyKeys),
                        kycFactIds,
                        kyaProfiles,
                        riskModel
                ));
    }

    public KycEvaluationResult evaluate(
            final String anEvaluationId,
            final AmlPartyId anAmlPartyId,
            final Set<PartyKey> partyKeys,
            final Set<String> kycFactIds,
            final Collection<KyaProfile> kyaProfiles,
            RiskModel riskModel
    ) {
        KycProfile kycProfile = new KycProfile(
                PartyContext.createPartyContext(
                        anAmlPartyId,
                        partyKeys),
                kycFactIds,
                kyaProfiles,
                riskModel);
        return kycProfile.evaluate(anEvaluationId);
    }
}
