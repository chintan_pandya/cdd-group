package com.botw.aml.sieve.eval.domain.model;

import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;

import java.util.Set;

/**
 * Created by krasnd52 on 12/6/15.
 */
public interface Evaluator {
    void evaluate(
            final RiskModel riskModel,
            final Set<String> factIds,
            KycEvaluationResult kycEvaluationResult);

}