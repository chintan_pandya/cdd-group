package com.botw.aml.sieve.eval.domain.model;

import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;

import java.util.Set;

/**
 * Created by krasnd52 on 12/6/15.
 */
public class RatingCalculator implements Evaluator {



    @Override
    public void evaluate(
            final RiskModel riskModel,
            final Set<String> factIds,
            KycEvaluationResult kycEvaluationResult) {

        if (kycEvaluationResult.getTotalScore() >= riskModel.getThreshold(Rating.HIGH))
            kycEvaluationResult.setRating(Rating.HIGH);
        else if (kycEvaluationResult.getTotalScore() >= riskModel.getThreshold(Rating.MODERATE))
            kycEvaluationResult.setRating(Rating.MODERATE);
        else if (kycEvaluationResult.getTotalScore() >= riskModel.getThreshold(Rating.LOW))
            kycEvaluationResult.setRating(Rating.LOW);
        else
            kycEvaluationResult.setRating(Rating.UNDEFINED);
    }

}
