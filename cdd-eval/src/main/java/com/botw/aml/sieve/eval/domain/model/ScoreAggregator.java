package com.botw.aml.sieve.eval.domain.model;

import com.botw.aml.sieve.eval.domain.model.account.KyaEvaluationResult;
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;

import java.util.Set;

/**
 * Created by krasnd52 on 12/13/15.
 */
public class ScoreAggregator implements Evaluator {

    @Override
    public void evaluate(
            final RiskModel riskModel,
            final Set<String> factIds,
            KycEvaluationResult kycEvaluationResult) {
        double totalScore = kycEvaluationResult.getKycResult().getScore();
        for (KyaEvaluationResult evaluationResult : kycEvaluationResult.getKyaResults()) {
            totalScore += evaluationResult.getScore();
        }
        kycEvaluationResult.setTotalScore(totalScore);
    }
}
