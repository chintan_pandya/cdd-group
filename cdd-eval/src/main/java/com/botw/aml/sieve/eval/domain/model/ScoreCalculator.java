package com.botw.aml.sieve.eval.domain.model;

import java.util.Collection;
import java.util.Set;


public final class ScoreCalculator {

    private EvaluationResult evaluationResult;

    public EvaluationResult evaluate(
            final RiskModel riskModel,
            final Set<String> factIds) {
        // System.out.println("starting evaluation");
        this.evaluationResult = new EvaluationResult(riskModel.convertQuestionCodetoRiskFactorCode(factIds));
        //this.evaluationResult = new EvaluationResult(factIds);
        this.processNode(riskModel.getRootRiskFactors(), riskModel, riskModel.convertQuestionCodetoRiskFactorCode(factIds));

        return this.evaluationResult;
    }

    private void processNode(Collection<RiskFactor> children, RiskModel riskModel, Set<String> factIds) {

        if (children == null){
            return;
        }
        for (RiskFactor childRiskFactor : children) {
            boolean isMissing = true;
            for (String factId : factIds) {
               // System.out.println("Fact Id: " + factId);
                for (RiskFactorValue riskFactorValue : childRiskFactor.getRiskFactorValues()) {
                 //   System.out.println("Risk Factor Value Id: " + riskFactorValue.id);
                    if (riskFactorValue.id.equals(factId)) {
                        isMissing = false;
                        this.evaluationResult.addFoundFact(factId+":"+(double)childRiskFactor.getScoreFor(riskFactorValue));
                        this.evaluationResult.removeFromSkippedInput(factId);
                        this.evaluationResult.increaseScore((double)childRiskFactor.getScoreFor(riskFactorValue));

                        processNode(riskFactorValue.getNextRiskFactors(), riskModel, factIds);
                    }
                }
            }
            if (isMissing) {
                this.evaluationResult.addMissingRiskFactor(childRiskFactor.getRiskFactorCode());
            }
        }
    }
}
