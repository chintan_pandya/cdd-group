package com.botw.aml.sieve.eval.domain.model.account;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by krasnd52 on 12/14/15.
 */
public final class AccountContext implements Serializable {
    private final Set<ProductKey> productKeys;
    private AccountKey accountKey;
    private AccountKey interimAccountKey;

    private AccountContext(
            AccountKey anAccountKey,
            AccountKey anInterimAccountKey,
            Set<ProductKey> productKeys) {
        if ((null == anInterimAccountKey) && (null == anAccountKey))
            throw new IllegalArgumentException("An interim account key and an account key must not both be null");
        if (null != anInterimAccountKey)
            this.interimAccountKey = anInterimAccountKey;
        if (null != anAccountKey)
            this.accountKey = anAccountKey;
        this.productKeys = productKeys;
    }

    public static AccountContext createInstance(
            AccountKey anAccountKey,
            AccountKey anInterimAccountKey,
            Set<ProductKey> productKeys
    ) {
        return new AccountContext(anAccountKey, anInterimAccountKey, productKeys);
    }

    public AccountKey getAccountKey() {
        return accountKey;
    }

    public AccountKey getInterimAccountKey() {
        return interimAccountKey;
    }


    public Set<ProductKey> getProductKeys() {
        return productKeys;
    }

}
