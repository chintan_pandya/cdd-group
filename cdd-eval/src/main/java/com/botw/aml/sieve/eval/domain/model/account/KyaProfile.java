package com.botw.aml.sieve.eval.domain.model.account;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by krasnd52 on 12/3/15.
 */
public final class KyaProfile implements Serializable {
    private final AccountContext accountContext;
    private final Set<String> kyaFactIds;

    private KyaProfile(
            AccountContext anAccountContext,
            Set<String> kyaFactIds) {
        this.accountContext = anAccountContext;
        this.kyaFactIds = kyaFactIds;
    }

    public static KyaProfile createKyaProfile(
            AccountContext anAccountContext,
            Set<String> kyaFactIds) {
        return new KyaProfile(anAccountContext, kyaFactIds);
    }

    public AccountContext getAccountContext() {
        return accountContext;
    }

    public Set<String> getKyaFactIds() {
        return kyaFactIds;
    }
}

