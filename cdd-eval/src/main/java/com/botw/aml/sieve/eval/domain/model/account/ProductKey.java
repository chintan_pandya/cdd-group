package com.botw.aml.sieve.eval.domain.model.account;

import com.botw.aml.sieve.eval.domain.model.infrastructure.Application;

import java.io.Serializable;

/**
 * Created by krasnd52 on 12/7/15.
 */
public final class ProductKey implements Serializable {
    private final String productId;
    private final String productCode;
    private final Application bookOfRecord;

    private ProductKey(String aProductId, String aProductCode, Application aBookOfRecord) {
        this.productId = aProductId;
        this.productCode = aProductCode;
        this.bookOfRecord = aBookOfRecord;
    }

    public static ProductKey createProductKey(String aProductId, String aProductCode, Application aBookOfRecord) {
        return new ProductKey(aProductId, aProductCode, aBookOfRecord);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductKey that = (ProductKey) o;
        return com.google.common.base.Objects.equal(getProductId(), that.getProductId()) &&
                com.google.common.base.Objects.equal(getProductCode(), that.getProductCode()) &&
                com.google.common.base.Objects.equal(getBookOfRecord(), that.getBookOfRecord());
    }

    @Override
    public int hashCode() {
        return com.google.common.base.Objects.hashCode(getProductId(), getProductCode(), getBookOfRecord());
    }

    public String getProductId() {


        return productId;
    }


    public String getProductCode() {
        return productCode;
    }


    public Application getBookOfRecord() {
        return bookOfRecord;
    }


}
