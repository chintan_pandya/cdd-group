package com.botw.aml.sieve.eval.domain.model.party;

import com.botw.aml.sieve.eval.domain.model.EvaluationResult;
import com.botw.aml.sieve.eval.domain.model.Rating;
import com.botw.aml.sieve.eval.domain.model.account.KyaEvaluationResult;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by krasnd52 on 12/13/15.
 */
@Entity
public final class KycEvaluationResult implements Serializable {

    private static final long serialVersionUID = 1L;//using DEFAULT!! value, see http://stackoverflow.com/questions/2121860/storing-objects-in-columns-using-hibernate-jpa

    @Id
    private String evaluationId;
    @Lob
    private EvaluationResult kycResult;

    @Lob
    private String amlPartyId; //TODO: this is for DEMO only!!

    @ElementCollection(targetClass=KyaEvaluationResult.class)
    private Collection<KyaEvaluationResult> kyaResults;
    private double totalScore;
    private Rating rating;
    private Date evaluationDate;

    @Lob
    private KycProfile kycProfile;

    protected KycEvaluationResult(){}; //for Spring Data CRUD repository

    public void setAmlPartyId(AmlPartyId amlPartyId) {
        this.amlPartyId = amlPartyId.id();
    }

    public KycEvaluationResult(String anEvaluationId, KycProfile kycProfile) {
        this.setAmlPartyId(kycProfile.getPartyContext().getAmlPartyId());
        this.setEvaluationId(anEvaluationId);
        this.kyaResults = new ArrayList<KyaEvaluationResult>();
        this.kycProfile =kycProfile;
    }

    public String getEvaluationId() {
        return evaluationId;
    }



    private KycEvaluationResult setEvaluationId(String anEvaluationId) {
        this.evaluationId = anEvaluationId;
        return this;
    }

    public Date getEvaluationDate() {
        return evaluationDate;
    }

    public KycEvaluationResult setEvaluationDate(Date evaluationDate) {
        this.evaluationDate = evaluationDate;
        return this;
    }

    public Rating getRating() {
        return rating;
    }

    public KycEvaluationResult setRating(Rating rating) {
        this.rating = rating;
        return this;
    }

    public double getTotalScore() {
        return totalScore;
    }

    public KycEvaluationResult setTotalScore(double totalScore) {
        this.totalScore = totalScore;
        return this;
    }

    public Collection<KyaEvaluationResult> getKyaResults() {
        return kyaResults;
    }

    public KycEvaluationResult addKyaResult(KyaEvaluationResult aKyaResult) {
        this.kyaResults.add(aKyaResult);
        return this;
    }

    public EvaluationResult getKycResult() {
        return kycResult;
    }

    public KycEvaluationResult setKycResult(EvaluationResult kycResult) {
        this.kycResult = kycResult;
        return this;
    }
    public String getAmlPartyId() {
        return kycProfile.getPartyContext().getAmlPartyId().id();//TODO: refactor
    }

    public KycProfile getKycProfile() {
        return kycProfile;
    }

    public void setKycProfile(KycProfile kycProfile) {
        this.kycProfile = kycProfile;
    }
}