package com.botw.aml.sieve.eval.domain.model.party;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * Created by krasnd52 on 12/8/15.
*/


@RepositoryRestResource(path = "evals",  collectionResourceRel = "evals" )
public interface KycEvaluationResultRepository extends CrudRepository<KycEvaluationResult, String>  {
    
//   void add(KycEvaluationResult anEvaluationResult);
//
//    Collection<KycEvaluationResult> allEvaluationResultsFor(AmlPartyId anAmlPartyId);
//
//    KycEvaluationResult evaluationWithId(String anEvaluationId);

    @RestResource(path = "byParty")
    List<KycEvaluationResult> findByAmlPartyId(@Param("id") String amlPartyId);

    @Override
    @RestResource(exported = false)
    void delete(String id);

    @Override
    @RestResource(exported = false)
    KycEvaluationResult save(KycEvaluationResult k);


    @Override
    @RestResource(exported = false)
    void delete(KycEvaluationResult entity);

    @Override
    @RestResource(exported = false)
    void delete(Iterable<? extends KycEvaluationResult> entities);


}

