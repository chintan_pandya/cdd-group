package com.botw.aml.sieve.eval.domain.model.party;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by marute01 on 01/22/2016.
 */

@Configuration
@EnableAutoConfiguration
//@EntityScan(basePackages = {"com.botw.aml.sieve.eval.domain.model.party"})
//@EnableJpaRepositories(basePackages = {"com.botw.aml.sieve.eval.domain.model.party"})
@EnableTransactionManagement
public class KycEvaluationResultRepositoryConfiguration {


}