package com.botw.aml.sieve.eval.domain.model.party;

import com.botw.aml.sieve.eval.domain.model.RiskModel;
import com.botw.aml.sieve.eval.domain.model.account.KyaProfile;
import com.botw.aml.sieve.eval.domain.model.RatingCalculator;
import com.botw.aml.sieve.eval.domain.model.ScoreAggregator;
import com.botw.aml.sieve.eval.domain.model.ScoreCalculator;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * Created by krasnd52 on 12/3/15.
 */
public final class KycProfile implements Serializable {

    @Autowired
    private RiskModel riskModel;

    private final PartyContext partyContext;
    private Collection<KyaProfile> kyaProfiles;
    private Set<String> kycFactIds;

    public KycProfile(
            PartyContext aPartyContext,
            Set<String> kycFactIds,
            Collection<KyaProfile> kyaProfiles,
            RiskModel riskModel
    ) {
        this.partyContext = aPartyContext;
        this.setKyaProfiles(kyaProfiles);
        this.setKycFactIds(kycFactIds);
        this.riskModel = riskModel;
    }

    public PartyContext getPartyContext() {
        return partyContext;
    }

    public Collection<KyaProfile> getKyaProfiles() {
        return kyaProfiles;
    }

    private void setKyaProfiles(Collection<KyaProfile> kyaProfiles) {
        this.kyaProfiles = kyaProfiles;
    }

    public Set<String> getKycFactIds() {
        return kycFactIds;
    }

    private void setKycFactIds(Set<String> kycFactIds) {
        this.kycFactIds = kycFactIds;
    }

    public KycEvaluationResult evaluate(String anEvaluationId) {

        KycEvaluationResult kycEvaluationResult = new KycEvaluationResult(anEvaluationId, this);

        // Evaluate kya profiles
        /* TODO: uncomment for including KYA results
        for (KyaProfile kyaProfile : this.getKyaProfiles()) {
            ScoreCalculator scoreCalculator = new ScoreCalculator();
            EvaluationResult evaluationResult = scoreCalculator.evaluate(
                    riskModel,
                    kyaProfile.getKyaFactIds());
            KyaEvaluationResult kyaEvaluationResult =
                    KyaEvaluationResult.createKyaEvaluationResult(
                            kyaProfile.getAccountContext())
                            .setEvaluationResult(evaluationResult);
            kycEvaluationResult.addKyaResult(kyaEvaluationResult);
        }
        */

        // Evaluate kyc profile
        kycEvaluationResult.setKycResult(
                new ScoreCalculator()
                        .evaluate(riskModel, this.getKycFactIds()));

        ScoreAggregator scoreAggregator = new ScoreAggregator();
        scoreAggregator.evaluate(riskModel, this.getKycFactIds(), kycEvaluationResult);

        RatingCalculator ratingCalculator = new RatingCalculator();
        ratingCalculator.evaluate(riskModel, this.getKycFactIds(), kycEvaluationResult);

        kycEvaluationResult.setEvaluationDate(new Date());


        return kycEvaluationResult;

    }
}
