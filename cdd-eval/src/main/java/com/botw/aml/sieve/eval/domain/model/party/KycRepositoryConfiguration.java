package com.botw.aml.sieve.eval.domain.model.party;

/**
 * Created by marute01 on 01/21/2016.
 * see https://springframework.guru/spring-boot-web-application-part-3-spring-data-jpa/

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.botw.aml.sieve.eval.domain.model.party"})
@EnableJpaRepositories(basePackages = {"com.botw.aml.sieve.eval.domain.model.party"})//TODO: refactor if we move repositories to another package
@EnableTransactionManagement
public class KycRepositoryConfiguration {
}
 */