package com.botw.aml.sieve.eval.domain.model.party;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by krasnd52 on 12/14/15.
 */
public final class PartyContext implements Serializable {
    private AmlPartyId amlPartyId;
    private Set<PartyKey> partyKeys;

    private PartyContext(
            AmlPartyId anAmlPartyId,
            Set<PartyKey> partyKeys) {
        if (null == anAmlPartyId && (null == partyKeys || partyKeys.isEmpty()))
            throw new IllegalArgumentException("globalPartyId and partyKeys must not both be null / empty");
        this.amlPartyId = anAmlPartyId;
        this.partyKeys = partyKeys;
    }

    public static PartyContext createPartyContext(AmlPartyId anAmlPartyId, Set<PartyKey> partyKeys) {
        return new PartyContext(anAmlPartyId, partyKeys);
    }

    public AmlPartyId getAmlPartyId() {
        return amlPartyId;
    }

    public Set<PartyKey> getPartyKeys() {
        return partyKeys;
    }
}
