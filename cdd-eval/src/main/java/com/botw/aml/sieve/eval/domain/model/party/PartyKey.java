package com.botw.aml.sieve.eval.domain.model.party;

import com.botw.aml.sieve.eval.domain.model.infrastructure.Application;
import com.google.common.base.Objects;

import java.io.Serializable;

/**
 * Created by krasnd52 on 12/7/15.
 */
public final class PartyKey implements Serializable {
    private String partyId;
    private Application bookOfRecord;

    public PartyKey(String aPartyId, Application aBookOfRecord) {

        this.setPartyId(aPartyId);
        this.setBookOfRecord(aBookOfRecord);
    }

    public String getPartyId() {
        return partyId;
    }

    private void setPartyId(String aPartyId) {
        this.partyId = aPartyId;
    }

    public Application getBookOfRecord() {
        return bookOfRecord;
    }

    private void setBookOfRecord(Application aBookOfRecord) {
        this.bookOfRecord = aBookOfRecord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PartyKey partyKey = (PartyKey) o;
        return Objects.equal(getPartyId(), partyKey.getPartyId()) &&
                Objects.equal(getBookOfRecord(), partyKey.getBookOfRecord());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getPartyId(), getBookOfRecord());
    }
}
