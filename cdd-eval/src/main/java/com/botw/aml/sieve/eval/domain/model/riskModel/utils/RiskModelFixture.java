package com.botw.aml.sieve.eval.domain.model.riskModel.utils;

import com.botw.aml.sieve.eval.domain.model.RiskFactor;
import com.botw.aml.sieve.eval.domain.model.RiskFactorValue;
import com.botw.aml.sieve.eval.domain.model.ScoreCalculator;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by marute01 on 01/29/2016.
 */
public class RiskModelFixture {

    public final Collection<RiskFactor> rootFactors;
    ScoreCalculator calculator;

    //CUSTOMER TYPE NODE
    public final String CUST_TYPE ="Customer Type Risk"; //customerType. Enum - individual or business

    public final String CUST_TYPE_IS_IND ="Individual";
    public final int CUST_TYPE_IS_IND_SCORE =0;

    public final String CUST_TYPE_IS_BUS ="Business";
    public final int CUST_TYPE_IS_BUS_SCORE =0;

    //BUSINESS COUNTRY NODE
    public final String BUS_COUNTRY_LOC ="Business Country Risk"; //countryLocation

    public final String BUS_COUNTRY_LOC_IS_IRAQ ="Iraq"; //US, Mexico
    public final int BUS_COUNTRY_LOC_IS_IRAQ_SCORE = 0;

    public final String BUS_COUNTRY_LOC_IS_CUBA ="Cuba";
    public final int BUS_COUNTRY_LOC_IS_CUBA_SCORE = 300;

    //Iraq City
    public final String IRAQ_City_LOC ="Iraq City"; //countryLocation

    public final String IRAQ_CITY_LOC_IS_BAGDAD ="Bagdad"; //US, Mexico
    public final int IRAQ_CITY_BAGDAD_SCORE = 100;

    public final String IRAQ_CITY_LOC_IS_MOSUL ="Mosul";
    public final int IRAQ_CITY_MOSUL_SCORE = 300;

    //Iraq State
    public final String IRAQ_STATE_LOC ="Iraq State"; //countryLocation

    public final String IRAQ_STATE_LOC_IS_XYZ ="XYZ"; //US, Mexico
    public final int IRAQ_STATE_XYZ_SCORE = 100;

    public final String IRAQ_STATE_LOC_IS_ABC ="ABC";
    public final int IRAQ_STATE_ABC_SCORE = 300;


    public RiskModelFixture(){
         this.rootFactors = new ArrayList<RiskFactor>();

        //Root Risk Factor
        RiskFactor customerType = new RiskFactor(CUST_TYPE,"Starting Point");
        this.rootFactors.add(customerType);

        RiskFactorValue customerTypeIsInd = new RiskFactorValue(CUST_TYPE_IS_IND, CUST_TYPE_IS_IND_SCORE, customerType);
        RiskFactorValue customerTypeIsBus = new RiskFactorValue(CUST_TYPE_IS_BUS, CUST_TYPE_IS_BUS_SCORE,customerType);

        customerType.addRiskFactorValue(customerTypeIsBus);
        customerType.addRiskFactorValue(customerTypeIsInd);

        //Countries of Business Risk Factor
        RiskFactor businessCountryLoc = new RiskFactor(BUS_COUNTRY_LOC,"Which countries do you do business with?");

        RiskFactorValue businessCountryLocIsIraq = new RiskFactorValue(BUS_COUNTRY_LOC_IS_IRAQ, BUS_COUNTRY_LOC_IS_IRAQ_SCORE, businessCountryLoc);
        RiskFactorValue businessCountryLocIsCuba = new RiskFactorValue(BUS_COUNTRY_LOC_IS_CUBA, BUS_COUNTRY_LOC_IS_CUBA_SCORE, businessCountryLoc);

        businessCountryLoc.addRiskFactorValue(businessCountryLocIsIraq);
        businessCountryLoc.addRiskFactorValue(businessCountryLocIsCuba);



        customerTypeIsBus.addNextRiskFactor(businessCountryLoc);

        //For Iraq State
        RiskFactor iraqStateLoc = new RiskFactor(IRAQ_STATE_LOC,"Which State do you do business in Iraq?");

        RiskFactorValue businessStateLocIsXYZ = new RiskFactorValue(IRAQ_STATE_LOC_IS_XYZ, IRAQ_STATE_XYZ_SCORE,iraqStateLoc );
        RiskFactorValue businessStateLocIsABC = new RiskFactorValue(IRAQ_STATE_LOC_IS_ABC, IRAQ_STATE_ABC_SCORE,iraqStateLoc );

        iraqStateLoc.addRiskFactorValue(businessStateLocIsXYZ);
        iraqStateLoc.addRiskFactorValue(businessStateLocIsABC);

        businessCountryLocIsIraq.addNextRiskFactor(iraqStateLoc);



        //For Iraq City
        RiskFactor iraqCityLoc = new RiskFactor(IRAQ_City_LOC,"Which city do you do business in Iraq?");

        RiskFactorValue businessCityLocIsBagdad = new RiskFactorValue(IRAQ_CITY_LOC_IS_BAGDAD, IRAQ_CITY_BAGDAD_SCORE,iraqCityLoc );
        RiskFactorValue businessCityLocIsMosul = new RiskFactorValue(IRAQ_CITY_LOC_IS_MOSUL, IRAQ_CITY_MOSUL_SCORE,iraqCityLoc );

        iraqCityLoc.addRiskFactorValue(businessCityLocIsBagdad);
        iraqCityLoc.addRiskFactorValue(businessCityLocIsMosul);

        businessCountryLocIsIraq.addNextRiskFactor(iraqCityLoc);

    }
    public Collection<RiskFactor> getRootFactors(){
        return this.rootFactors;
    }
}
