package com.botw.aml.sieve.eval.domain.model.riskModel.utils;

import com.botw.aml.sieve.eval.domain.model.RiskModel;
import com.botw.aml.sieve.eval.domain.model.RiskFactor;
import com.botw.aml.sieve.eval.domain.model.RiskFactorData;
import com.botw.aml.sieve.eval.util.TreeNode;
import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by pandyc01 on 01/14/2016.
 */

@Component
public class RiskModelParser {

    private Map<String, TreeNode<RiskFactorData>> tree;
    private List<RiskModelRow> rmList;
    private List<TreeNode<RiskFactorData>> treeNodeList;

    @Autowired
    private ResourceLoader resourceLoader;


    public RiskModelParser (){

    }


    public Collection<RiskFactor> loadRiskModelFromFile(String xlFileName){
        Collection<RiskFactor> riskFactors = new ArrayList<RiskFactor>();

        List<RiskModelRow> rmList = parseExcel(xlFileName);

        for (RiskModelRow rmRow : rmList) {

        }

        return riskFactors;
    }

    public List<RiskModelRow> parseExcel(String path){
        List<RiskModelRow> rmList = new ArrayList<RiskModelRow>();
        RiskModel riskModel=null;
        try {
            Resource resource = resourceLoader.getResource("classpath:" +path);
            File inputWorkbook = resource.getFile();
            Workbook w;
            w = Workbook.getWorkbook(inputWorkbook);
            Sheet sheet = w.getSheet("Question if Else Rules");    // Get the first sheet

            for (int row = 1; row < sheet.getRows(); row++) {   //sheet.getRows()
                RiskModelRow riskModelRow = new RiskModelRow();
                for (int col = 0; col < sheet.getColumns(); col++) {   //sheet.getColumns()
                    Cell cell = sheet.getCell(col, row);
                    CellType type = cell.getType();
                    System.out.print(cell.getContents() + " | ");

                    if (col == 0) {
                        if (cell.getContents().equals(null) || cell.getContents() == "") {
                            break;
                        }
                    }
                    if (col == 1) {
                        riskModelRow.setQuestion(cell.getContents());
                    }
                    if (col == 2) {
                        riskModelRow.setAnswer(cell.getContents());
                    }
                   /* if (col == 3) {
                        riskModelRow.setScore(Double.parseDouble(cell.getContents()));
                    }*/
                    if (col == 4) {
                        riskModelRow.setNextQuestion(cell.getContents());
                    }

                    riskModelRow.setScore(0.0);
                }

                rmList.add(riskModelRow);
                System.out.println("::::"+rmList.size());
            }
        }catch (BiffException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }

        return rmList;

    }
}
