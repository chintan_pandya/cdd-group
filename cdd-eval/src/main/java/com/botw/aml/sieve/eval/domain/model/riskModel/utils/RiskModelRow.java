package com.botw.aml.sieve.eval.domain.model.riskModel.utils;

/**
 * Created by pandyc01 on 01/14/2016.
 */
public class RiskModelRow {

    String question;
    String answer;
    Double score;
    String nextQuestion;

    String getId(){
        return question + answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getNextQuestion() {
        return nextQuestion;
    }

    public void setNextQuestion(String nextQuestion) {
        this.nextQuestion = nextQuestion;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }
}
