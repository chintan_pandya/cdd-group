package com.botw.aml.sieve.eval.util;

/**
 * Created by krasnd52 on 12/4/15.
 */
public class TreeNode<Data> {
    private Data data;
    private TreeNode<Data> firstChild;
    private TreeNode<Data> nextSibling;

    public TreeNode(Data data) {
        this.data = data;
        firstChild = null;
        nextSibling = null;
    }

    public Data getData() {
        return data;
    }

    public TreeNode<Data> setData(Data data) {
        this.data = data;
        return this;
    }

    public TreeNode<Data> getFirstChild() {
        return firstChild;
    }

    public TreeNode<Data> setFirstChild(TreeNode<Data> firstChild) {
        this.firstChild = firstChild;
        return this;
    }

    public TreeNode<Data> getNextSibling() {
        return nextSibling;
    }

    public TreeNode<Data> setNextSibling(TreeNode<Data> nextSibling) {
        this.nextSibling = nextSibling;
        return this;
    }

    public boolean isLeaf() {
        return null == getFirstChild();
    }

    public boolean isLastSibling() {
        return null == getNextSibling();
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "data=" + data +
                ", firstChild=" + firstChild +
                ", nextSibling=" + nextSibling +
                '}';
    }
}
