package com.botw.cdd.model;

import com.botw.aml.sieve.eval.domain.model.Rating;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2016-01-23T03:31:37.392Z")
public class CddEvaluationResult {
  
 /* public enum RiskLevelEnum {
     unacceptable,  high,  medium,  low,  insufficient_info, 
  };*/
  private Rating rating = null;
  private RiskScore riskScore = null;
  private CddProfileFindings findings = null;
  private RiskModel riskModel = null;
  private String timestamp = null;

  
  /**
   * AML risk level of a party relationship for the bank.  * unacceptable - when encountered a hard stop(s)  * high * medium * low * insufficient _info - when one or more required facts are missing from the CDD profile
   **/
  @ApiModelProperty(value = "AML risk level of a party relationship for the bank.  * unacceptable - when encountered a hard stop(s)  * high * medium * low * insufficient _info - when one or more required facts are missing from the CDD profile")
  @JsonProperty("risk_level")
  public Rating getRating() {
    return rating;
  }
  public void setRating(Rating rating) {
    this.rating = rating;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("risk_score")
  public RiskScore getRiskScore() {
    return riskScore;
  }
  public void setRiskScore(RiskScore riskScore) {
    this.riskScore = riskScore;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("findings")
  public CddProfileFindings getFindings() {
    return findings;
  }
  public void setFindings(CddProfileFindings findings) {
    this.findings = findings;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("risk_model")
  public RiskModel getRiskModel() {
    return riskModel;
  }
  public void setRiskModel(RiskModel riskModel) {
    this.riskModel = riskModel;
  }

  
  /**
   * Full date and time when the evaluation task completed.
   **/
  @ApiModelProperty(value = "Full date and time when the evaluation task completed.")
  @JsonProperty("timestamp")
  public String getTimestamp() {
    return timestamp;
  }
  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
    //System.out.println("timestamp"+timestamp.toString());
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CddEvaluationResult cddEvaluationResult = (CddEvaluationResult) o;
    return Objects.equals(rating, cddEvaluationResult.rating) &&
        Objects.equals(riskScore, cddEvaluationResult.riskScore) &&
        Objects.equals(findings, cddEvaluationResult.findings) &&
        Objects.equals(riskModel, cddEvaluationResult.riskModel) &&
        Objects.equals(timestamp, cddEvaluationResult.timestamp);
  }

  @Override
  public int hashCode() {
    return Objects.hash(rating, riskScore, findings, riskModel, timestamp);
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class CddEvaluationResult {\n");
    
    sb.append("  riskLevel: ").append(rating).append("\n");
    sb.append("  riskScore: ").append(riskScore).append("\n");
    sb.append("  findings: ").append(findings).append("\n");
    sb.append("  riskModel: ").append(riskModel).append("\n");
    sb.append("  timestamp: ").append(timestamp).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
