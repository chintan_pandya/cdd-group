package com.botw.cdd.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2016-01-23T03:31:37.392Z")
public class CddEvaluationTask {
  
  private String evalId = null;
  private String pingAfter = null;

  
  /**
   * Unique identifier of the evaluation task
   **/
  @ApiModelProperty(value = "Unique identifier of the evaluation task")
  @JsonProperty("eval_id")
  public String getEvalId() {
    return evalId;
  }
  public void setEvalId(String evalId) {
    this.evalId = evalId;
  }

  
  /**
   * An estimated time of completion of the CDD evaluation.
   **/
  @ApiModelProperty(value = "An estimated time of completion of the CDD evaluation.")
  @JsonProperty("ping_after")
  public String getPingAfter() {
    return pingAfter;
  }
  public void setPingAfter(String pingAfter) {
    this.pingAfter = pingAfter;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CddEvaluationTask cddEvaluationTask = (CddEvaluationTask) o;
    return Objects.equals(evalId, cddEvaluationTask.evalId) &&
        Objects.equals(pingAfter, cddEvaluationTask.pingAfter);
  }

  @Override
  public int hashCode() {
    return Objects.hash(evalId, pingAfter);
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class CddEvaluationTask {\n");
    
    sb.append("  evalId: ").append(evalId).append("\n");
    sb.append("  pingAfter: ").append(pingAfter).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
