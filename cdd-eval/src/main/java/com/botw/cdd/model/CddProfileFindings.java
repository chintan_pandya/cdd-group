package com.botw.cdd.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * A container object for the findings derived during the risk evaluation of the provided CDD Profile.
 **/
@ApiModel(description = "A container object for the findings derived during the risk evaluation of the provided CDD Profile.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2016-01-23T03:31:37.392Z")
public class CddProfileFindings  {
  
  private FactGroupFindings kycFindings = null;
  private List<FactGroupFindings> kyaFindings = new ArrayList<FactGroupFindings>();

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("kyc_findings")
  public FactGroupFindings getKycFindings() {
    return kycFindings;
  }
  public void setKycFindings(FactGroupFindings kycFindings) {
    this.kycFindings = kycFindings;
  }

  
  /**
   * An `Findings` object for each KYA profile included in the evaluation request
   **/
  @ApiModelProperty(value = "An `Findings` object for each KYA profile included in the evaluation request")
  @JsonProperty("kya_findings")
  public List<FactGroupFindings> getKyaFindings() {
    return kyaFindings;
  }
  public void setKyaFindings(List<FactGroupFindings> kyaFindings) {
    this.kyaFindings = kyaFindings;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CddProfileFindings cddProfileFindings = (CddProfileFindings) o;
    return Objects.equals(kycFindings, cddProfileFindings.kycFindings) &&
        Objects.equals(kyaFindings, cddProfileFindings.kyaFindings);
  }

  @Override
  public int hashCode() {
    return Objects.hash(kycFindings, kyaFindings);
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class CddProfileFindings {\n");
    
    sb.append("  kycFindings: ").append(kycFindings).append("\n");
    sb.append("  kyaFindings: ").append(kyaFindings).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
