package com.botw.cdd.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


/**
 * Findings from an evaluation that provide context for the risk score and level.
 **/
@ApiModel(description = "Findings from an evaluation that provide context for the risk score and level.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2016-01-23T03:31:37.392Z")
  public class FactGroupFindings {
  
  private String groupKey = null;
  private Set<String> hardStopFacts = new HashSet<String>();
  private Set<String> missingFactCategories = new HashSet<String>();
  private Set<String> skippedFacts = new HashSet<String>();
  private Set<String> usedFacts = new HashSet<String>();
  private Date timestamp = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("group_key")
  public String getGroupKey() {
    return groupKey;
  }
  public void setGroupKey(String groupKey) {
    this.groupKey = groupKey;
  }

  
  /**
   * One or more facts that elevate the risk level of the relationship with the party to unacceptable. \nMultiple hard stops in a single CDD profile are uncommon (due to the nature of the rule: encountering one should halt the onboarding process) but not impossible. It could happen if the onboarding workflow doesn't correctly align with the risk model, or in the course of ongoing CDD maintenance after new hard stops added to the risk model.
   **/
  @ApiModelProperty(value = "One or more facts that elevate the risk level of the relationship with the party to unacceptable. \nMultiple hard stops in a single CDD profile are uncommon (due to the nature of the rule: encountering one should halt the onboarding process) but not impossible. It could happen if the onboarding workflow doesn't correctly align with the risk model, or in the course of ongoing CDD maintenance after new hard stops added to the risk model.")
  @JsonProperty("hard_stop_facts")
  public Set<String> getHardStopFacts() {
    return hardStopFacts;
  }
  public void setHardStopFacts(Set<String> hardStopFacts) {
    this.hardStopFacts = hardStopFacts;
  }

  
  /**
   * A set of required fact categories that are missing from a CDD profile. For example, if the risk model views CountryOfResidence as a required fact category and a CDD profile includes no facts that speak for the party's country of residence, PartyCountryOfResidence will be included in this set.\nThe desired outcome is for this set to be empty. It reflects completeness of the CddProfile used in the evaluation.\nWhen not empty, the risk level for the CDD profile is evaluated to `insufficient_info`. The CDD profile should be amended with missing information and resubmitted for evaluation.
   **/
  @ApiModelProperty(value = "A set of required fact categories that are missing from a CDD profile. For example, if the risk model views CountryOfResidence as a required fact category and a CDD profile includes no facts that speak for the party's country of residence, PartyCountryOfResidence will be included in this set.\nThe desired outcome is for this set to be empty. It reflects completeness of the CddProfile used in the evaluation.\nWhen not empty, the risk level for the CDD profile is evaluated to `insufficient_info`. The CDD profile should be amended with missing information and resubmitted for evaluation.")
  @JsonProperty("missing_fact_categories")
  public Set<String> getMissingFactCategories() {
    return missingFactCategories;
  }
  public void setMissingFactCategories(Set<String> missingFactCategories) {
    this.missingFactCategories = missingFactCategories;
  }

  
  /**
   * A set of facts included in the evaluation request that were not required by the risk model, and, therefore, were skipped by Sieve. These facts didn't figure in the evaluation result.\nSkipped facts are benign from the CDD scoring perspective, and provided here for information purposes only. There is no benefit to resubmitting a CDD Profile minus these facts, as they do not impact CDD score and risk level.
   **/
  @ApiModelProperty(value = "A set of facts included in the evaluation request that were not required by the risk model, and, therefore, were skipped by Sieve. These facts didn't figure in the evaluation result.\nSkipped facts are benign from the CDD scoring perspective, and provided here for information purposes only. There is no benefit to resubmitting a CDD Profile minus these facts, as they do not impact CDD score and risk level.")
  @JsonProperty("skipped_facts")
  public Set<String> getSkippedFacts() {
    return skippedFacts;
  }
  public void setSkippedFacts(Set<String> skippedFacts) {
    this.skippedFacts = skippedFacts;
  }


  @ApiModelProperty(value = "Facts that were used for calculating the Score")
  @JsonProperty("used_facts")
  public Set<String> getUsedFacts() {
    return usedFacts;
  }
  public void setUsedFacts(Set<String> usedFacts) {
    this.usedFacts = usedFacts;
  }

  
  /**
   * Full date and time when the evaluation of this fact group completed. Note that this timestamp may substantially precede the timestamp of the parent CddEvaluationResult, as Sieve may cache an evaluation result for a fact group, and use it in future evaluations as long as the risk model doesn't change.
   **/
  @ApiModelProperty(value = "Full date and time when the evaluation of this fact group completed. Note that this timestamp may substantially precede the timestamp of the parent CddEvaluationResult, as Sieve may cache an evaluation result for a fact group, and use it in future evaluations as long as the risk model doesn't change.")
  @JsonProperty("timestamp")
  public Date getTimestamp() {
    return timestamp;
  }
  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FactGroupFindings factGroupFindings = (FactGroupFindings) o;
    return Objects.equals(groupKey, factGroupFindings.groupKey) &&
        Objects.equals(hardStopFacts, factGroupFindings.hardStopFacts) &&
        Objects.equals(missingFactCategories, factGroupFindings.missingFactCategories) &&
        Objects.equals(skippedFacts, factGroupFindings.skippedFacts) &&
        Objects.equals(timestamp, factGroupFindings.timestamp);
  }

  @Override
  public int hashCode() {
    return Objects.hash(groupKey, hardStopFacts, missingFactCategories, skippedFacts, timestamp);
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class FactGroupFindings {\n");
    
    sb.append("  groupKey: ").append(groupKey).append("\n");
    sb.append("  hardStopFacts: ").append(hardStopFacts).append("\n");
    sb.append("  missingFactCategories: ").append(missingFactCategories).append("\n");
    sb.append("  skippedFacts: ").append(skippedFacts).append("\n");
    sb.append("  timestamp: ").append(timestamp).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
