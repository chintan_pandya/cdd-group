package com.botw.cdd.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;


/**
 * Metadata for the risk model that produced the evaluation.
 **/
@ApiModel(description = "Metadata for the risk model that produced the evaluation.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2016-01-23T03:31:37.392Z")
public class RiskModel  {
  
  private String name = null;
  private String version = null;

  
  /**
   * A human-friendly label for the risk model. \n    \nThis field is not part of the risk model versioning scheme.
   **/
  @ApiModelProperty(value = "A human-friendly label for the risk model. \n    \nThis field is not part of the risk model versioning scheme.")
  @JsonProperty("name")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  
  /**
   * Sieve follows a sequence-based versioning scheme of [major.minor.revision].\n    \n* The major number is increased when the change may cause incompatibility with interfacing systems; for example, a new fact type added to the model. \n    \n* The minor number is incremented when only minor features or significant fixes have been added; for example, a weights changing for some facts. \n    \n* The revision number is incremented when minor bugs are fixed.
   **/
  @ApiModelProperty(value = "Sieve follows a sequence-based versioning scheme of [major.minor.revision].\n    \n* The major number is increased when the change may cause incompatibility with interfacing systems; for example, a new fact type added to the model. \n    \n* The minor number is incremented when only minor features or significant fixes have been added; for example, a weights changing for some facts. \n    \n* The revision number is incremented when minor bugs are fixed.")
  @JsonProperty("version")
  public String getVersion() {
    return version;
  }
  public void setVersion(String version) {
    this.version = version;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RiskModel riskModel = (RiskModel) o;
    return Objects.equals(name, riskModel.name) &&
        Objects.equals(version, riskModel.version);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, version);
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class RiskModel {\n");
    
    sb.append("  name: ").append(name).append("\n");
    sb.append("  version: ").append(version).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
