package com.botw.cdd.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2016-01-23T03:31:37.392Z")
public class RiskScore  {
  
  private Double totalScore = null;
  private FactGroupScore kycResult = null;
  private List<FactGroupScore> kyaResults = new ArrayList<FactGroupScore>();

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("total_score")
  public Double getTotalScore() {
    return totalScore;
  }
  public void setTotalScore(Double totalScore) {
    this.totalScore = totalScore;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("kyc_result")
  public FactGroupScore getKycResult() {
    return kycResult;
  }
  public void setKycResult(FactGroupScore kycResult) {
    this.kycResult = kycResult;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("kya_results")
  public List<FactGroupScore> getKyaResults() {
    return kyaResults;
  }
  public void setKyaResults(List<FactGroupScore> kyaResults) {
    this.kyaResults = kyaResults;
  }

  

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RiskScore riskScore = (RiskScore) o;
    return Objects.equals(totalScore, riskScore.totalScore) &&
        Objects.equals(kycResult, riskScore.kycResult) &&
        Objects.equals(kyaResults, riskScore.kyaResults);
  }

  @Override
  public int hashCode() {
    return Objects.hash(totalScore, kycResult, kyaResults);
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class RiskScore {\n");
    
    sb.append("  totalScore: ").append(totalScore).append("\n");
    sb.append("  kycResult: ").append(kycResult).append("\n");
    sb.append("  kyaResults: ").append(kyaResults).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
