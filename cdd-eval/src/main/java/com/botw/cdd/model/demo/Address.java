package com.botw.cdd.model.demo;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by manij01 on 01/26/2016.
 */
@Embeddable
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String addressId;
    public String street;
    public String city;
    public String State;

}
