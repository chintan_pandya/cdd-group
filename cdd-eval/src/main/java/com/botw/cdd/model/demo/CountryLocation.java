package com.botw.cdd.model.demo;

import java.io.Serializable;

/**
 * Created by manij01 on 01/27/2016.
 */
public enum CountryLocation implements Serializable {
    //DL,ST,CL,AL,AM,BE

    Iraq {
        @Override
        public String toString() {
            return "Iraq";
        }
    },
    Cuba {
        @Override
        public String toString() {
            return "Cuba";
        }
    },
    Afghanistan {
        @Override
        public String toString() {
            return "Afghanistan";
        }
    },
    India {
        @Override
        public String toString() {
            return "India";
        }
    }
}
