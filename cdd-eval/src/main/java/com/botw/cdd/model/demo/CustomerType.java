package com.botw.cdd.model.demo;

import java.io.Serializable;

/**
 * Created by manij01 on 01/27/2016.
 */
public enum CustomerType implements Serializable {
    //DL,ST,CL,AL,AM,BE

    Individual {
        @Override
        public String toString() {
            return "Individual";
        }
    },
    Business {
        @Override
        public String toString() {
            return "Business";
        }
    }
}
