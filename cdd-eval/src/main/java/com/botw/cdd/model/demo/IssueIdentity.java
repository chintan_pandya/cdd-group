package com.botw.cdd.model.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by manij01 on 01/27/2016.
 */
@Entity
public class IssueIdentity implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public String Id;
    //public Date issuedDate;
    public String issuedByState;
    public IssueIdentityType type;
    //public String otherType;

/*    public Date getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(Date issuedDate) {
        this.issuedDate = issuedDate;
    }*/

    public String getIssuedByState() {
        return issuedByState;
    }

    public void setIssuedByState(String issuedByState) {
        this.issuedByState = issuedByState;
    }

    public IssueIdentityType getType() {
        return type;
    }

    public void setType(IssueIdentityType type) {
        this.type = type;
    }
/*
    public String getOtherType() {
        return otherType;
    }

    public void setOtherType(String otherType) {
        this.otherType = otherType;
    }*/

}
