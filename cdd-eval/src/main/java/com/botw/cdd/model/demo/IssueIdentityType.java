package com.botw.cdd.model.demo;

import java.io.Serializable;

/**
 * Created by manij01 on 01/27/2016.
 */
public enum IssueIdentityType implements Serializable {
    //DL,ST,CL,AL,AM,BE

    DL {
        @Override
        public String toString() {
            return "DL";
        }
    },
    ST {
        @Override
        public String toString() {
            return "ST";
        }
    },
    CL {
        @Override
        public String toString() {
            return "CL";
        }
    },
    AL {
        @Override
        public String toString() {
            return "AL";
        }
    },
    AM {
        @Override
        public String toString() {
            return "AM";
        }
    },
    BE {
        @Override
        public String toString() {
            return "BE";
        }
    }
}
