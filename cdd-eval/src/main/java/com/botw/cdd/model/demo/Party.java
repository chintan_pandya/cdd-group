package com.botw.cdd.model.demo;

import javax.persistence.*;
import java.util.ArrayList;

/**
 * Created by manij01 on 01/26/2016.
 */

@Entity
public class Party {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String partyId;
    //private String amlPartyId;
    /*public String orgCountry;
    public String orgLegalForm;
    public String orgLegalFormOther;
    public String orgPubliclyTraded;
    public String orgStockSymbol;
    public String issuedIdentIssuerState;
    public String orgStockExchange;
    public String orgBusinessIdentificationOther;
    public String orgState;
    public String politicallyExposedPersonFamilyMemberPEPFlag;

    public String foreignBusinessUSEntityYes;
    public String foreignBusinessHeadquarterCountry;
    public String foreignBusinessPhysicalLocationCountry;
    public String politicallyExposedPersonPositionClassification;
    public String politicallyExposedPersonPositionClassificationOther;
    public String politicallyExposedPersonPositionStatus;
    public String politicallyExposedPersonGovLevel;
    public String politicallyExposedPersonLastServiceDt;
    public String politicallyExposedPersonPositionDuration;
    public String politicallyExposedPersonOverseeGovAssets;
    public String politicallyExposedPersonFullNameRCA;
    public String issuedIdentIssueDt;*/


    //private List<IssueIdentType> issuedIdentType;
    @Lob
    public ArrayList<IssueIdentity> issuedIdentity = new ArrayList<IssueIdentity>();
    public CustomerType customerType;
    public CountryLocation countryLocation;
    public String foreignBusinessPhysLocOuytsideUS;

    public CountryLocation getCountryLocation() {
        return countryLocation;
    }

    public void setCountryLocation(CountryLocation countryLocation) {
        this.countryLocation = countryLocation;
    }

    public CustomerType getCustomerType() {
        return customerType;
    }

    public void setCustomerType(CustomerType customerType) {
        this.customerType = customerType;
    }



    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

/*    public String getAmlPartyId() {
        return amlPartyId;
    }

    public void setAmlPartyId(String amlPartyId) {
        this.amlPartyId = amlPartyId;
    }*/

    public ArrayList<IssueIdentity> getIssuedIdentity() {
        return issuedIdentity;
    }

    public void setIssuedIdentity(ArrayList<IssueIdentity> issuedIdentity) {
        this.issuedIdentity = issuedIdentity;
    }

    public String getForeignBusinessPhysLocOuytsideUS() {
        return foreignBusinessPhysLocOuytsideUS;
    }

    public void setForeignBusinessPhysLocOuytsideUS(String foreignBusinessPhysLocOuytsideUS) {
        this.foreignBusinessPhysLocOuytsideUS = foreignBusinessPhysLocOuytsideUS;
    }

    /*public String getOrgCountry() {
        return orgCountry;
    }

    public void setOrgCountry(String orgCountry) {
        this.orgCountry = orgCountry;
    }

    public String getOrgLegalForm() {
        return orgLegalForm;
    }

    public void setOrgLegalForm(String orgLegalForm) {
        this.orgLegalForm = orgLegalForm;
    }

    public String getOrgLegalFormOther() {
        return orgLegalFormOther;
    }

    public void setOrgLegalFormOther(String orgLegalFormOther) {
        this.orgLegalFormOther = orgLegalFormOther;
    }

    public String getOrgPubliclyTraded() {
        return orgPubliclyTraded;
    }

    public void setOrgPubliclyTraded(String orgPubliclyTraded) {
        this.orgPubliclyTraded = orgPubliclyTraded;
    }

    public String getOrgStockSymbol() {
        return orgStockSymbol;
    }

    public void setOrgStockSymbol(String orgStockSymbol) {
        this.orgStockSymbol = orgStockSymbol;
    }

    public String getIssuedIdentIssuerState() {
        return issuedIdentIssuerState;
    }

    public void setIssuedIdentIssuerState(String issuedIdentIssuerState) {
        this.issuedIdentIssuerState = issuedIdentIssuerState;
    }

    public String getOrgStockExchange() {
        return orgStockExchange;
    }

    public void setOrgStockExchange(String orgStockExchange) {
        this.orgStockExchange = orgStockExchange;
    }

    public String getOrgBusinessIdentificationOther() {
        return orgBusinessIdentificationOther;
    }

    public void setOrgBusinessIdentificationOther(String orgBusinessIdentificationOther) {
        this.orgBusinessIdentificationOther = orgBusinessIdentificationOther;
    }

    public String getOrgState() {
        return orgState;
    }

    public void setOrgState(String orgState) {
        this.orgState = orgState;
    }




    public String getPoliticallyExposedPersonFamilyMemberPEPFlag() {
        return politicallyExposedPersonFamilyMemberPEPFlag;
    }

    public void setPoliticallyExposedPersonFamilyMemberPEPFlag(String politicallyExposedPersonFamilyMemberPEPFlag) {
        this.politicallyExposedPersonFamilyMemberPEPFlag = politicallyExposedPersonFamilyMemberPEPFlag;
    }


      public String getForeignBusinessUSEntityYes() {
        return foreignBusinessUSEntityYes;
    }

    public void setForeignBusinessUSEntityYes(String foreignBusinessUSEntityYes) {
        this.foreignBusinessUSEntityYes = foreignBusinessUSEntityYes;
    }

    public String getForeignBusinessHeadquarterCountry() {
        return foreignBusinessHeadquarterCountry;
    }

    public void setForeignBusinessHeadquarterCountry(String foreignBusinessHeadquarterCountry) {
        this.foreignBusinessHeadquarterCountry = foreignBusinessHeadquarterCountry;
    }

    public String getForeignBusinessPhysicalLocationCountry() {
        return foreignBusinessPhysicalLocationCountry;
    }

    public void setForeignBusinessPhysicalLocationCountry(String foreignBusinessPhysicalLocationCountry) {
        this.foreignBusinessPhysicalLocationCountry = foreignBusinessPhysicalLocationCountry;
    }

    public String getPoliticallyExposedPersonPositionClassification() {
        return politicallyExposedPersonPositionClassification;
    }

    public void setPoliticallyExposedPersonPositionClassification(String politicallyExposedPersonPositionClassification) {
        this.politicallyExposedPersonPositionClassification = politicallyExposedPersonPositionClassification;
    }

    public String getPoliticallyExposedPersonPositionClassificationOther() {
        return politicallyExposedPersonPositionClassificationOther;
    }

    public void setPoliticallyExposedPersonPositionClassificationOther(String politicallyExposedPersonPositionClassificationOther) {
        this.politicallyExposedPersonPositionClassificationOther = politicallyExposedPersonPositionClassificationOther;
    }

    public String getPoliticallyExposedPersonPositionStatus() {
        return politicallyExposedPersonPositionStatus;
    }

    public void setPoliticallyExposedPersonPositionStatus(String politicallyExposedPersonPositionStatus) {
        this.politicallyExposedPersonPositionStatus = politicallyExposedPersonPositionStatus;
    }

    public String getPoliticallyExposedPersonGovLevel() {
        return politicallyExposedPersonGovLevel;
    }

    public void setPoliticallyExposedPersonGovLevel(String politicallyExposedPersonGovLevel) {
        this.politicallyExposedPersonGovLevel = politicallyExposedPersonGovLevel;
    }

    public String getPoliticallyExposedPersonLastServiceDt() {
        return politicallyExposedPersonLastServiceDt;
    }

    public void setPoliticallyExposedPersonLastServiceDt(String politicallyExposedPersonLastServiceDt) {
        this.politicallyExposedPersonLastServiceDt = politicallyExposedPersonLastServiceDt;
    }

    public String getPoliticallyExposedPersonPositionDuration() {
        return politicallyExposedPersonPositionDuration;
    }

    public void setPoliticallyExposedPersonPositionDuration(String politicallyExposedPersonPositionDuration) {
        this.politicallyExposedPersonPositionDuration = politicallyExposedPersonPositionDuration;
    }

    public String getPoliticallyExposedPersonOverseeGovAssets() {
        return politicallyExposedPersonOverseeGovAssets;
    }

    public void setPoliticallyExposedPersonOverseeGovAssets(String politicallyExposedPersonOverseeGovAssets) {
        this.politicallyExposedPersonOverseeGovAssets = politicallyExposedPersonOverseeGovAssets;
    }

    public String getPoliticallyExposedPersonFullNameRCA() {
        return politicallyExposedPersonFullNameRCA;
    }

    public void setPoliticallyExposedPersonFullNameRCA(String politicallyExposedPersonFullNameRCA) {
        this.politicallyExposedPersonFullNameRCA = politicallyExposedPersonFullNameRCA;
    }
*/
}

