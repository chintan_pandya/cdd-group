/*
package com.botw.aml.sieve.eval.port.adapter.model.demo;

import com.botw.aml.sieve.eval.application.EvaluationApplicationService;
import com.botw.aml.sieve.eval.application.commands.EvaluateCommand;
import com.botw.aml.sieve.eval.application.commands.InitiateEvaluationCommand;
import Evaluation;
import CddEvaluationTask;
import CddProfile;
import FactGroup;
import com.botw.aml.sieve.eval.util.SieveConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

*/
/**
 * Created by marute01 on 01/26/2016.
 *//*

@RepositoryEventHandler
@Component
public class PartyEventHandler {


    @Autowired
    EvaluationApplicationService evaluationApplicationService;

    @HandleBeforeSave
    //@HandleBeforeCreate
    @HandleAfterCreate
    public void handlePartySave(Party p) throws Exception {
        // … you can now deal with Party in a type-safe way
        System.out.println("Inside Party"+p.toString());
      */
/*  if (p.orgName.length()<3){

            throw new Exception("Name must be more than 3 characters long");
        }*//*


        CddProfile cddProfile = new CddProfile();
        FactGroup kycFactGroup = new FactGroup();

        System.out.println("********PartyID*"+p.getPartyId());
        kycFactGroup.setGroupKey(p.getPartyId());
        Set<String> kycFactKeys = populateCddProfile(p);

        List<FactGroup> kyaFactGroups = new ArrayList<FactGroup>();
        kyaFactGroups.add(new FactGroup());

        kycFactGroup.setFactKeys(kycFactKeys);

        cddProfile.setPartyFacts(kycFactGroup);
        cddProfile.setAccountFacts(kyaFactGroups);

        System.out.println("*************cddProfile***********"+cddProfile);
        System.out.println("********afterPartyID*"+p.getPartyId());

        submitEvaluation(cddProfile);

    }

    @HandleBeforeSave
    public void handleProfileSave(Address p) {
        // … you can now deal with Address in a type-safe way
    }
    //setting the factIds(Question+answer)
    private Set<String>populateCddProfile(Party p)
    {
        Set<String>factIds=new HashSet<String>();
       */
/* if(""!=p.getOrgCountry())factIds.add(SieveConstants.orgCountry+p.getOrgCountry());
        if(""!=p.getOrgLegalForm())factIds.add(SieveConstants.orgLegalForm+p.getOrgLegalForm());
        if(""!=p.getOrgLegalFormOther())factIds.add(SieveConstants.orgLegalFormOther+p.getOrgLegalFormOther());
        if(""!=p.getOrgPubliclyTraded())factIds.add(SieveConstants.orgPubliclyTraded+p.getOrgPubliclyTraded());
        if(""!=p.getOrgStockSymbol())factIds.add(SieveConstants.orgStockSymbol+p.getOrgStockSymbol());
        if(""!=p.getOrgBusinessIdentificationOther())factIds.add(SieveConstants.orgBusinessIdentificationOther+p.getOrgBusinessIdentificationOther());
        if(""!=p.getOrgStockExchange())factIds.add(SieveConstants.orgStockExchange+p.getOrgStockExchange());
        // if(""!=p.getIssuedIdentIssuerState())factIds.add(SieveConstants.issuedIdentIssuerState+p.getIssuedIdentIssuerState());
        if(""!=p.getOrgState())factIds.add(SieveConstants.orgState+p.getOrgState());
        if(""!=p.getForeignBusinessUSEntityYes())factIds.add(SieveConstants.foreignBusinessUSEntityYes+p.getForeignBusinessUSEntityYes());
        if(""!=p.getForeignBusinessHeadquarterCountry())factIds.add(SieveConstants.foreignBusinessHeadquarterCountry+p.getForeignBusinessHeadquarterCountry());
        if(""!=p.getForeignBusinessPhysicalLocationCountry())factIds.add(SieveConstants.foreignBusinessPhysicalLocationCountry+p.getForeignBusinessPhysicalLocationCountry());
        if(""!=p.getPoliticallyExposedPersonPositionClassification())factIds.add(SieveConstants.politicallyExposedPersonPositionClassification+p.getPoliticallyExposedPersonPositionClassification());
        if(""!=p.getPoliticallyExposedPersonPositionClassificationOther())factIds.add(SieveConstants.politicallyExposedPersonPositionClassificationOther+p.getPoliticallyExposedPersonPositionClassificationOther());
        if(""!=p.getPoliticallyExposedPersonPositionStatus())factIds.add(SieveConstants.politicallyExposedPersonPositionStatus+p.getPoliticallyExposedPersonPositionStatus());
        if(""!=p.getPoliticallyExposedPersonGovLevel())factIds.add(SieveConstants.politicallyExposedPersonGovLevel+p.getPoliticallyExposedPersonGovLevel());
        if(""!=p.getPoliticallyExposedPersonLastServiceDt())factIds.add(SieveConstants.politicallyExposedPersonLastServiceDt+p.getPoliticallyExposedPersonLastServiceDt());*//*

        if(null!=p.getIssuedIdentity() && p.getIssuedIdentity().size()>0)
        {
            List<IssueIdentity> issuedIdentityList = p.getIssuedIdentity();
            IssueIdentity issueIdentity=null;

            for (int i = 0; i < issuedIdentityList.size(); i++)
            {
                issueIdentity=issuedIdentityList.get(i);
                if(null!=issueIdentity)
                {
                    if(""!=issueIdentity.getIssuedByState())factIds.add(SieveConstants.issuedIdentIssuerState+":"+issueIdentity.getIssuedByState());
                    //if(null!=issueIdentity.getIssuedDate())factIds.add(SieveConstants.issuedDate+issueIdentity.getIssuedDate().toString());

                    if(null!=issueIdentity.getType())
                    {
                        String issueType=issueIdentity.getType().toString();
                        System.out.println("&&&&&&&issueType&&&&&&"+SieveConstants.issueIdentityType+issueType);
                        if(null!=issueIdentity.getType())factIds.add(SieveConstants.issueIdentityType+":"+issueType);
                    }
                }
            }

        }
        if(null!=p.getCustomerType())
        {
            CustomerType custType=p.getCustomerType();
            String custTypeVal=custType.toString();

            if(null!=custType)factIds.add(SieveConstants.CUST_TYPE+":"+custTypeVal);
        }
        if(null!=p.getCountryLocation())
        {
            CountryLocation countryLocation=p.getCountryLocation();
            String countryVal=countryLocation.toString();
            System.out.println("countryVal"+countryVal);
            if(null!=countryLocation)factIds.add(SieveConstants.BUS_COUNTRY_LOC+":"+countryVal);

        }
        //if(""!=p.politicallyExposedPersonFamilyMemberPEPFlag)factIds.add(SieveConstants.politicallyExposedPersonFamilyMemberPEPFlag+p.getPoliticallyExposedPersonFamilyMemberPEPFlag());
        if(""!=p.getForeignBusinessPhysLocOuytsideUS())factIds.add(SieveConstants.foreignBusinessPhysLocOuytsideUS+":"+p.getForeignBusinessPhysLocOuytsideUS());
        System.out.println("*******factIds************"+factIds);
        return factIds;
    }


    private void submitEvaluation(CddProfile cddProfile)
    {
        System.out.println("*****Before InitiateEvaluationCommand********"+cddProfile.getPartyFacts().getGroupKey());
        InitiateEvaluationCommand initiateEvaluationCommand = new InitiateEvaluationCommand(cddProfile.getPartyFacts().getGroupKey(),
                null,null,cddProfile.getPartyFacts().getFactKeys());
        Evaluation evaluation = this.evaluationApplicationService.initiateEvaluation(initiateEvaluationCommand);

        EvaluateCommand evaluateCommand = new EvaluateCommand(evaluation.getEvaluationId().id(),initiateEvaluationCommand);
        this.evaluationApplicationService.evaluate(evaluateCommand);

        CddEvaluationTask task = new CddEvaluationTask();
        task.setEvalId(evaluation.getEvaluationId().id());

       */
/* Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
        calendar.add(Calendar.SECOND, 2);
        task.setPingAfter(calendar.getTime().toString());*//*


        final Date currentTime = new Date();
        final SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy hh:mm:ss a z");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        task.setPingAfter(sdf.format(currentTime));

        System.out.println("********task EvalID*********"+task.getEvalId()+"PArtyID"+cddProfile.getPartyFacts().getGroupKey());

    }
}
*/
