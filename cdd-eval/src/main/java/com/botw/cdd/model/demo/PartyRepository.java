package com.botw.cdd.model.demo;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by manij01 on 01/26/2016.
 */
public interface PartyRepository extends CrudRepository <Party, String>
{

}
