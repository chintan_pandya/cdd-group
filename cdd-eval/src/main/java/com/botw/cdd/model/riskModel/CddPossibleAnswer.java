package com.botw.cdd.model.riskModel;

import java.io.Serializable;

/**
 * Created by marute01 on 03/06/2016.
 */
public class CddPossibleAnswer implements Serializable {

    public final String answerCode;
    public final String answerText;

    public CddPossibleAnswer(String answerCode, String answerText) {
        this.answerCode = answerCode;
        this.answerText = answerText;
    }
}
