package com.botw.cdd.model.riskModel;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by marute01 on 03/06/2016.
 */
public class CddProvidedAnswer implements Serializable {

    @Getter @Setter private  String questionCode;
    @Getter @Setter private  List<String> providedAnswerCodes;

    public CddProvidedAnswer (){
    }

    public CddProvidedAnswer(String questionCode, List<String> providedAnswerCodes) {
        this.questionCode = questionCode;
        this.providedAnswerCodes = providedAnswerCodes;
    }
}

