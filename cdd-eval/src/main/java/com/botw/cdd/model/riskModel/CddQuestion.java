package com.botw.cdd.model.riskModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by marute01 on 03/06/2016.
 */

public class CddQuestion implements Serializable {

    public final String questionCode;
    public final String questionType;
    public final String questionText;

    public final List<CddPossibleAnswer> possibleAnswers;
    public final CddPossibleAnswer defaultAnswer;

    public CddQuestion(String questionCode, String questionType, String questionText, List<CddPossibleAnswer> possibleAnswers, CddPossibleAnswer defaultAnswer) {
        this.questionCode = questionCode;
        this.questionType = questionType;
        this.questionText = questionText;
        this.possibleAnswers = possibleAnswers;
        this.defaultAnswer = defaultAnswer;
    }
}
