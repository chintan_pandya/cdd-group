package com.botw.cdd.rest;

import com.botw.aml.sieve.eval.application.EvaluationApplicationService;
import com.botw.aml.sieve.eval.application.commands.EvaluateCommand;
import com.botw.aml.sieve.eval.application.commands.InitiateEvaluationCommand;
import com.botw.aml.sieve.eval.application.representations.EvaluationWithDetailsRepresentation;
import com.botw.aml.sieve.eval.common.serializer.ObjectSerializer;
import com.botw.aml.sieve.eval.domain.model.Evaluation;
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;
import com.botw.cdd.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by krasnd52 on 12/8/15.
 * An Evaluation adapter for the RESTful HTTP port provided by the JAX-RS implementation
 * <p/>
 * It delegates to the application service EvaluationService.
 */

@RestController
public class EvaluationResource extends AbstractResource  {

    private RepositoryEntityLinks entityLinks;

    @Autowired
    public EvaluationResource(RepositoryEntityLinks entityLinks) {
        this.entityLinks = entityLinks;
    }

    @Autowired
    EvaluationApplicationService evaluationApplicationService;//TO  DO: return back to using the registry Removed for demo only






    @RequestMapping(value= "/evaluation/{evalId}",
            method= RequestMethod.GET,
            produces = "application/hal+json")
    public CddEvaluationResult getEvaluation(@PathVariable("evalId") String anEvaluationId,
                                             @Context Request aRequest){

        KycEvaluationResult evaluationResult = this.evaluationApplicationService.evaluationResultOf(anEvaluationId);

        if (evaluationResult == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        CddEvaluationResult cddEvaluationResult=this.populateCddEvaluationResult(evaluationResult);
        return cddEvaluationResult;
    }

    @RequestMapping(value= "/sampleCddProfile/0",
            method= RequestMethod.GET,
            produces = "application/hal+json")
    public CddProfile getSampleCddProfile(){

        CddProfile cddProfile = new CddProfile();

        FactGroup kycFactGroup = new FactGroup();
        kycFactGroup.setGroupKey("RMID:002047234138823");
        Set<String> kycFactKeys = new HashSet<String>();
        kycFactKeys.add("SomeRiskFact:123");
        kycFactKeys.add("SomeRiskFact:234");
        kycFactKeys.add("SomeRiskFact:567");


        List<FactGroup> kyaFactGroups = new ArrayList<FactGroup>();
        kyaFactGroups.add(new FactGroup());

        kycFactGroup.setFactKeys(kycFactKeys);

        cddProfile.setPartyFacts(kycFactGroup);
        cddProfile.setAccountFacts(kyaFactGroups);



        return cddProfile;
    }

    @RequestMapping(value= "/submitEvaluation",
            method= RequestMethod.POST,
            produces = "application/json")
    public CddEvaluationTask submitEvaluation (@RequestBody CddProfile cddProfile){

        InitiateEvaluationCommand initiateEvaluationCommand = new InitiateEvaluationCommand(cddProfile.getPartyFacts().getGroupKey(),
                null,null,cddProfile.getPartyFacts().getFactKeys());
        Evaluation evaluation = this.evaluationApplicationService.initiateEvaluation(initiateEvaluationCommand);

        EvaluateCommand evaluateCommand = new EvaluateCommand(evaluation.getEvaluationId().id(),initiateEvaluationCommand);
        this.evaluationApplicationService.evaluate(evaluateCommand);

        CddEvaluationTask task = new CddEvaluationTask();
        task.setEvalId(evaluation.getEvaluationId().id());

       /* Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
        calendar.add(Calendar.SECOND, 2);
        task.setPingAfter(calendar.getTime().toString());*/

        final Date currentTime = new Date();
        final SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy hh:mm:ss a z");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        task.setPingAfter(sdf.format(currentTime));

        return task;
    }

    @RequestMapping(value= "/evaluations/{partyId}",
            method= RequestMethod.GET,
            produces = "application/hal+json")
    public List<CddEvaluationResult> getEvaluations(@PathVariable("partyId") String partyId)

    {
        System.out.println("*********Inside getEvaluation********************"+partyId);

        List<KycEvaluationResult> evaluationResults = this.evaluationApplicationService.evaluationResultOfParty(partyId);

        if (evaluationResults == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        List<CddEvaluationResult> cddEvaluationResults=this.populateCddEvaluationResults(evaluationResults);
        //  System.out.println("************CddEvaluationResult*********"+cddEvaluationResult);
        return cddEvaluationResults;
    }


    private Response evaluationResponse(Request aRequest, KycEvaluationResult anEvaluationResult) {

        Response response;

        EntityTag eTag =
                //new EntityTag(Integer.toString(anEvaluationResult.hashCode()));
                this.evaluationETag(anEvaluationResult);
        Response.ResponseBuilder conditionalBuilder=null;

        conditionalBuilder = aRequest.evaluatePreconditions(eTag);


        if (conditionalBuilder != null) {
            response =
                    conditionalBuilder
                            .cacheControl(this.cacheControlFor(3600))
                            .tag(eTag)
                            .build();
        } else {
            String representation =
                    ObjectSerializer
                            .instance()
                            .serialize(new EvaluationWithDetailsRepresentation(anEvaluationResult));

            response =
                    Response
                            .ok(representation)
                            .cacheControl(this.cacheControlFor(3600))
                            .tag(eTag)
                            .build();
        }

        return response;
    }

    private CddEvaluationResult populateCddEvaluationResult(KycEvaluationResult  kycEvaluationResult){
        CddEvaluationResult cddEvaluationResult=new CddEvaluationResult();
        RiskScore riskScore=new RiskScore();
        CddProfileFindings cddProfileFindings=new CddProfileFindings();
        FactGroupFindings factGroupFindings=new FactGroupFindings();
        FactGroupScore factGroupScore=new FactGroupScore();

        factGroupScore.setTotalScore(kycEvaluationResult.getTotalScore());
        //factGroupScore.setGroupKey(kycEvaluationResult.getAmlPartyId().toString());
        riskScore.setKycResult(factGroupScore);

        riskScore.setTotalScore(kycEvaluationResult.getTotalScore());
        cddEvaluationResult.setRiskScore(riskScore);
        cddEvaluationResult.setRating(kycEvaluationResult.getRating());

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy hh:mm:ss a z");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        cddEvaluationResult.setTimestamp(sdf.format(kycEvaluationResult.getEvaluationDate()).toString());


        factGroupFindings.setMissingFactCategories(this.evaluationApplicationService.getQuestionCode(kycEvaluationResult.getKycResult().getMissingRiskFactors(),false));
        factGroupFindings.setSkippedFacts(this.evaluationApplicationService.getQuestionCode(kycEvaluationResult.getKycResult().getSkippedInput(),true));
        factGroupFindings.setUsedFacts(this.evaluationApplicationService.getQuestionCode(kycEvaluationResult.getKycResult().getUsedInput(),false));

        /*factGroupFindings.setMissingFactCategories(kycEvaluationResult.getKycResult().getMissingRiskFactors());
        factGroupFindings.setSkippedFacts(kycEvaluationResult.getKycResult().getSkippedInput());
        factGroupFindings.setUsedFacts(kycEvaluationResult.getKycResult().getUsedInput());*/
        //factGroupFindings.setHardStopFacts(kycEvaluationResult.getKycResult().getUsedInput());
        cddProfileFindings.setKycFindings(factGroupFindings);
        cddEvaluationResult.setFindings(cddProfileFindings);

        return cddEvaluationResult;
    }

    private List<CddEvaluationResult> populateCddEvaluationResults(List<KycEvaluationResult>  kycEvaluationResults)
    {
        List<CddEvaluationResult> cddEvaluationResults = new ArrayList<CddEvaluationResult>();
        for (KycEvaluationResult kycEvaluationResult: kycEvaluationResults){

            cddEvaluationResults.add(populateCddEvaluationResult(kycEvaluationResult));
        }

        return cddEvaluationResults;
    }



}
