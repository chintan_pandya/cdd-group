package com.botw.cdd.rest;

import com.botw.aml.sieve.eval.application.EvaluationApplicationService;
import com.botw.cdd.model.riskModel.CddQuestion;
import com.botw.cdd.model.riskModel.CddProvidedAnswer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;


@RestController
public class QuestionBankResource extends AbstractResource {

    private RepositoryEntityLinks entityLinks;

    @Autowired
    EvaluationApplicationService evaluationApplicationService;

       @RequestMapping(value= "/nextQuestions",
            method= RequestMethod.POST)
    public Collection<CddQuestion> getNextQuestions(@RequestBody CddProvidedAnswer cddProvidedAnswer){

        return this.evaluationApplicationService.nextQuestions(cddProvidedAnswer);
    }


}
