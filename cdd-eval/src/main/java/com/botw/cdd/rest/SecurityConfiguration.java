package com.botw.cdd.rest;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


/**
 * Created by marute01 on 01/21/2016.
 * https://springframework.guru/using-the-h2-database-console-in-spring-boot-with-spring-security/
 */

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests().antMatchers("/").permitAll().and()
                .authorizeRequests().antMatchers("/console/**").permitAll();//TODO: turn off or modify for prod

        httpSecurity.csrf().disable();
        httpSecurity.headers().frameOptions().disable();
    }

}
