package com.botw.aml.sieve.eval;

//import com.botw.aml.sieve.eval.domain.model.party.KycRepositoryConfiguration;

/**
 * Created by marute01 on 01/21/2016.



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {KycRepositoryConfiguration.class})
public class KycEvaluationResultRepositoryTest {

    private KycEvaluationResultRepository kycEvaluationResultRepository;

    @Autowired
    public void setKycEvaluationResultRepository(KycEvaluationResultRepository kycEvaluationResultRepository) {
        this.kycEvaluationResultRepository = kycEvaluationResultRepository;
    }

    @Test
    public void testSaveEvaluationResult(){
        //setup evalResult
        KycEvaluationResult evalResult = new KycEvaluationResult("123");


        //save evalResult, verify has ID value after save
        assertNull(evalResult.getEvaluationId()); //null before save
        kycEvaluationResultRepository.save(evalResult);
        assertNotNull(evalResult.getEvaluationId()); //not null after save

        //fetch from DB
        KycEvaluationResult fetchedProduct = kycEvaluationResultRepository.findOne(evalResult.getEvaluationId());

        //should not be null
        assertNotNull(fetchedProduct);

        //should equal
        assertEquals(evalResult.getEvaluationId(), fetchedProduct.getEvaluationId());



        //verify count of products in DB
        long evalResultsCount = kycEvaluationResultRepository.count();
        assertEquals(evalResultsCount, 1);

        //get all products, list should only have one
        Iterable<KycEvaluationResult> kycEvaluationResults = kycEvaluationResultRepository.findAll();

        int count = 0;

        for(KycEvaluationResult p : kycEvaluationResults){
            count++;
        }

        assertEquals(count, 1);
    }
}

*/