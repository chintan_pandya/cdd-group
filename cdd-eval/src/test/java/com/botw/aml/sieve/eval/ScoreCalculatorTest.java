package com.botw.aml.sieve.eval;


/**
 * Created by marute01 on 01/28/2016.
 */
public class ScoreCalculatorTest{// extends TestCase {

   /* ScoreCalculator calculator;
    RiskModel riskModel;
    RiskModelFixture fx;


    @Before
    public void setUp() throws Exception {
        this.fx = new RiskModelFixture();
       // this.riskModel = new RiskModel();
        this.calculator = new ScoreCalculator();


    }

    public void testBasicCase (){

        Set<String> factIds = new HashSet<String>();
        Fact validFact = new Fact(fx.CUST_TYPE,fx.CUST_TYPE_IS_IND);
        factIds.add(validFact.getUri());


        EvaluationResult indResult = calculator.evaluate(riskModel,factIds);
        assertEquals("Used input check", factIds,indResult.getUsedInput());
        assertEquals("Total score check", (double)fx.CUST_TYPE_IS_IND_SCORE, indResult.getScore());

    }
    public void testScoresAddedCorrectly (){

        Set<String> factIds = new HashSet<String>();
        Fact busCustomer = new Fact(fx.CUST_TYPE,fx.CUST_TYPE_IS_BUS);
        Fact hasBusinessInCuba = new Fact(fx.BUS_COUNTRY_LOC,fx.BUS_COUNTRY_LOC_IS_CUBA);

        factIds.add(busCustomer.getUri());
        factIds.add(hasBusinessInCuba.getUri());

        EvaluationResult indResult = calculator.evaluate(riskModel,factIds);
        assertEquals("Total score check", (double)fx.CUST_TYPE_IS_BUS_SCORE + fx.BUS_COUNTRY_LOC_IS_CUBA_SCORE, indResult.getScore());

    }

    public void testMissingFacts (){

        Fact whenFact = new Fact(fx.CUST_TYPE,fx.CUST_TYPE_IS_BUS);
        System.out.println("Test when fact id is required but missing: ");

        Set<String> factIds = new HashSet<String>();
        factIds.add(whenFact.getUri());

        EvaluationResult resultWithMissedFactor = calculator.evaluate(riskModel,factIds);

        Set<String> missingFactors = new HashSet<String>();
        missingFactors.add(fx.BUS_COUNTRY_LOC);

        assertEquals("Expect a missing factor", missingFactors,resultWithMissedFactor.getMissingRiskFactors());
        assertEquals("Used input check", factIds,resultWithMissedFactor.getUsedInput());

        Fact aRequiredFact = new Fact(fx.BUS_COUNTRY_LOC,fx.BUS_COUNTRY_LOC_IS_CUBA);
        System.out.println("Test when required fact id is added");

        factIds.add(aRequiredFact.getUri());
        EvaluationResult resultWithNoMissedFactors = calculator.evaluate(riskModel,factIds);
        assertEquals("Expect no missing factors",new HashSet<String>(),resultWithNoMissedFactors.getMissingRiskFactors());

    }
    public void testSkippedInput (){

        Fact whenFact = new Fact(fx.CUST_TYPE,fx.CUST_TYPE_IS_IND);
        System.out.println("Test when one or more fact ids are present but are not necessary");

        Set<String> factIds = new HashSet<String>();

        Fact toBeSkippedFact1 = new Fact(fx.BUS_COUNTRY_LOC,fx.BUS_COUNTRY_LOC_IS_CUBA);
        Fact toBeSkippedFact2 = new Fact(fx.BUS_COUNTRY_LOC,fx.BUS_COUNTRY_LOC_IS_IRAQ);

        factIds.add(whenFact.getUri());
        factIds.add(toBeSkippedFact1.getUri());
        factIds.add(toBeSkippedFact2.getUri());


        EvaluationResult resultWithSkippedFacts = calculator.evaluate(riskModel,factIds);

        Set<String> skippedFactIds = new HashSet<String>();
        skippedFactIds.add(toBeSkippedFact1.getUri());
        skippedFactIds.add(toBeSkippedFact2.getUri());

        Set<String> usedFactIds = new HashSet<String>();
        usedFactIds.add(whenFact.getUri());

        assertEquals("Expect a skipped factor list", skippedFactIds,resultWithSkippedFacts.getSkippedInput());
        assertEquals("Used input check", usedFactIds,resultWithSkippedFacts.getUsedInput());


    }

*/

}
