package com.botw.cdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CddInfoServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CddInfoServiceApplication.class, args);
	}
}
