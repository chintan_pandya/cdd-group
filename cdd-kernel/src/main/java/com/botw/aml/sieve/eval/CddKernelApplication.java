package com.botw.aml.sieve.eval;

/**
 * Created by pandyc01 on 04/03/2016.
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CddKernelApplication {

    public static void main(String[] args) {
        SpringApplication.run(CddKernelApplication.class, args);
    }
}