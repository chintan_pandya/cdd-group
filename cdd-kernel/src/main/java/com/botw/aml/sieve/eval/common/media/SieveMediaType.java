package com.botw.aml.sieve.eval.common.media;


public class SieveMediaType {


    public static final String EVAL_TYPE =
            "application/vnd.sieve.eval+json";

    public static final String PARTY_DATA_MGMT_TYPE =
            "application/vnd.sieve.party_data_mgmt+json";
}