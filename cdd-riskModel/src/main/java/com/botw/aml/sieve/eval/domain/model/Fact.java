package com.botw.aml.sieve.eval.domain.model;

import com.google.common.base.Objects;

import java.util.UUID;

/**
 * Created by krasnd52 on 12/10/15.
 */
public final class Fact {
    /**
     * A unique identifier of the fact obtained from a sequence.
     */
    private final String id;
    /**
     * A machine-friendly label of the fact, e.g.
     * "/party/address/country/123"
     */
    private final String uri;
    /**
     * Succinct human-friendly description of the fact, e.g. "US resident"
     */
    private final String questionCode;  //right now storing RM questions into this
    /**
     * Verbose human-friendly description of the fact, e.g. "The party is a US resident"
     */
    private final String answerCode;   //right now stroing RM answers into this
    /**
     * The risk measure assigned to the fact by the risk model
     */

    public Fact(String questionCode, String answerCode) {
        this.id =UUID.randomUUID().toString();//TODO refactor with possible different UUID
        this.questionCode = questionCode;
        this.answerCode = answerCode;
        this.uri = questionCode + ":" + answerCode;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fact fact = (Fact) o;
        return  Objects.equal(getUri(), fact.getUri()) &&
                Objects.equal(getQuestionCode(), fact.getQuestionCode()) &&
                Objects.equal(getAnswerCode(), fact.getAnswerCode());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode( getUri(), getQuestionCode(), getAnswerCode());
    }

    public String getUri() {
        return uri;
    }

    public String getQuestionCode() {
        return questionCode;
    }

    public String getAnswerCode() {
        return answerCode;
    }


}
