package com.botw.aml.sieve.eval.domain.model;

import java.io.Serializable;

/**
 * Created by krasnd52 on 12/6/15.
 */
public enum Rating implements Serializable {
    UNDEFINED, LOW, MODERATE, HIGH
}

