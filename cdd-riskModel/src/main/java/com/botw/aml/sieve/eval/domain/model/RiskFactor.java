package com.botw.aml.sieve.eval.domain.model;

import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by marute01 on 01/28/2016.
 */
public class RiskFactor implements Serializable {

    @Getter public final String questionCode;
    @Getter public final String riskFactorCode;

    private Collection<RiskFactorValue> riskFactorValues;

    public RiskFactor(String questionCode, String riskFactorCode) {
        this.questionCode = questionCode;
        this.riskFactorCode=riskFactorCode;
        this.riskFactorValues = new ArrayList<RiskFactorValue>();
    }

    public void addRiskFactorValue( RiskFactorValue riskFactorValue){
        this.riskFactorValues.add(riskFactorValue);
    }

    public int getScoreFor(RiskFactorValue riskFactorValue){
        for(RiskFactorValue value: this.riskFactorValues ){
            if (value.getCode().equals(riskFactorValue.getCode())) return value.getScore();
        }
        return 0;
    }

    public Collection<RiskFactorValue> getRiskFactorValues (){
        return this.riskFactorValues;
    }
}
