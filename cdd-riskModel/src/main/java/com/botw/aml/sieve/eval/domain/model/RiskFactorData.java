package com.botw.aml.sieve.eval.domain.model;

/**
 * Created by krasnd52 on 12/14/15.
 */
public interface RiskFactorData {
    Fact fact();

    double riskWeight();
}
