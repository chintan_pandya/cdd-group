package com.botw.aml.sieve.eval.domain.model;

/**
 * Created by pandyc01 on 12/21/2015.
 */
public class RiskFactorDataImpl implements RiskFactorData {


    private Fact fact;
    private double riskWeight;

    public RiskFactorDataImpl(Fact fact, double riskWeight){
        this.fact =fact;
        this.riskWeight= riskWeight;
    }
    public RiskFactorDataImpl(Fact fact){
        this.fact =fact;
        this.riskWeight= 1; //default value
    }

    public Fact fact(){  // todo remove public
        return fact;
    }
    public double riskWeight(){
        return riskWeight;
    }


}
