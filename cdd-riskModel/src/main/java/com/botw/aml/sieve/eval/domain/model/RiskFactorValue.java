package com.botw.aml.sieve.eval.domain.model;

import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by marute01 on 01/28/2016.
 */
public class RiskFactorValue implements Serializable {


    @Getter private final String code;
    @Getter private final int score;

    private Collection<RiskFactor> nextRiskFactors;
    private final RiskFactor parentRiskFactor;
    public final String id;


    public RiskFactorValue(String code, int score,RiskFactor parentRiskFactor) {
        this.code = code;
        this.score = score;

        this.parentRiskFactor = parentRiskFactor;
        this.id = parentRiskFactor.getRiskFactorCode() + ":" + code;
        this.nextRiskFactors = new ArrayList<RiskFactor>();
    }

    public void addNextRiskFactor(RiskFactor riskFactor){
        this.nextRiskFactors.add(riskFactor);
    }



    public Collection<RiskFactor> getNextRiskFactors() {
        return nextRiskFactors;
    }

    public void setNextRiskFactors(Collection<RiskFactor> nextRiskFactors) {
        this.nextRiskFactors = nextRiskFactors;
    }



}
