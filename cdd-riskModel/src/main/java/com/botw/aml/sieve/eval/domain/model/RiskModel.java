package com.botw.aml.sieve.eval.domain.model;


import com.botw.aml.sieve.eval.domain.model.admin.Answer;
import com.botw.aml.sieve.eval.domain.model.admin.NextQuestionRule;
import com.botw.aml.sieve.eval.domain.model.admin.Question;
import com.botw.aml.sieve.eval.domain.model.admin.RiskScoreRule;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import java.io.Serializable;
import java.util.*;

/**
 * Created by marute01
 *
 * Risk Model is not equal to Q/A Model. They are separate!!!
 *
 * Bottom line the *risk model* may have more factors than the *qa* model actually asks,
 * in other words some facts are received from "question answer", others are derived from other places.
 * These "derived" facts come from "un asked" questions.
 *
 * To explain, consider the 2 examples:
 *
 * Example 1. a "simple" risk model is: If x=a then score 100. In this case the "risk model" and the "q/a" model
 * dependencies are identical but what about this:
 * Example 2. a more complicated risk model: If x=a and y=b and z=c, then score is 10,000.
 * Here, if x=a, then ask y, but z=c comes as a completely
 * different "fact" -not from q/a, but, for example, from observations of actual transactions for the
 * customer.
 *
 * Consider if this observation should be true given above: (think more)
 * 1. Risk Model is the best to be modeled using rules engine to implement complex combinations of facts
 * 2. Q/A Model is a simple "if else, then" flow
 *
 *
 */

public class RiskModel implements Serializable {

    @Value("${cdd.sieve.risk-model.mode}")
    private String mode;

    public final String name;
    public final String version;


    private Collection<RiskFactor> rootRiskFactors;

    private Iterable <Question> questions;
    private Iterable <Answer> answers;
    private Iterable <RiskScoreRule> riskScoreRules;
    private Iterable <NextQuestionRule> nextQuestionRules;

    private Map<String, List<Question>> nextQuestionsRulesMap;

    public RiskModel(Iterable<Question> questions,
                     Iterable<Answer> answers,
                     Iterable<RiskScoreRule> riskScoreRules,
                     Iterable<NextQuestionRule> nextQuestionRules,
                     String name, String version) {
        this.questions = questions;
        this.answers = answers;
        this.riskScoreRules = riskScoreRules;
        this.nextQuestionRules = nextQuestionRules;
        this.name = name;
        this.version = version;

        this.setNextQuestionRulesMap();
        this.setPossibleAnswersForQuestions();
        this.rootRiskFactors = new ArrayList<RiskFactor>();
        buildRiskFactorsFromQuestions(getRootQuestionsWithRiskFactors(), null, true);
    }

    public double getThreshold(Rating rating) {
        double threshold;
        switch (rating) {
            case LOW:
                threshold = 0; //TODO move thresholds into a config file / db
                break;
            case MODERATE:
                threshold = 500; //TODO move thresholds into a config file / db
                break;
            case HIGH:
                threshold = 1000; //TODO move thresholds into a config file / db
                break;
            case UNDEFINED:
                threshold = Double.MIN_VALUE;
                break;
            default:
                throw new IllegalArgumentException("unsupported rating");
        }
        return threshold;
    }

    public String name() {
        return this.name;
    }

    public String version() {
        return this.version;
    }


    public Collection<RiskFactor> getRootRiskFactors(){
        return this.rootRiskFactors;
    }

    private int getScoreFor(String riskFactorCode,String riskFactorValueCode){

        for (RiskScoreRule rule : this.riskScoreRules){
            if (rule.getRiskFactorCode().equals(riskFactorCode)&&
                    rule.getRiskFactorValueCode().equals(riskFactorValueCode)){
                return rule.getScore();
            }
        }
        return 0;
    }

    public static String getQAKey(Question question, Answer answer){
        return getQAKeyBasedOnCodes(question.getQuestionCode(),answer.getAnswerCode());
    }

    public static String getQAKeyBasedOnCodes(String questionCode, String answerCode){
        return questionCode.concat("::").concat(answerCode);
    }

    public List<Question> getNextQuestions(String questionCode, Iterable<String> providedAnswerCodes) {

        List<Question> nextQuestions = new ArrayList<Question>();

        for (String providedAnswerCode: providedAnswerCodes){
            List <Question> nextQuestionList = nextQuestionsRulesMap.get(getQAKeyBasedOnCodes(questionCode,providedAnswerCode));
            if(null!=nextQuestionList){
            for (Question nextQuestion: nextQuestionList) {
                if (nextQuestion != null) {
                    nextQuestions.add(nextQuestion);
                }
            }
        }}
        return nextQuestions;
    }


    public List<Question> getNextQuestionsWithRiskFactors(String questionCode, Iterable<String> providedAnswerCodes) {


        List<Question> nextQuestions = new ArrayList<Question>();
        for (Question question: getNextQuestions(questionCode,providedAnswerCodes)){
            if (question.getRiskFactorCode() !=null){
                nextQuestions.add(question);
            }
        }
        return nextQuestions;
    }

    public List<Question> getRootQuestions(){

        List<Question> rootQuestions = new ArrayList<Question>();
        for (Question question: this.questions){
             if (null!=question.getIsRoot() && question.getIsRoot()){
                rootQuestions.add(question);
            }
        }
        return rootQuestions;
    }

    public List<Question> getRootQuestionsWithRiskFactors(){

        List<Question> rootQuestionsWithRiskFactors = new ArrayList<Question>();
        for (Question question : getRootQuestions()){
            if (question.getRiskFactorCode()!=null){
                rootQuestionsWithRiskFactors.add(question);
            }
        }
        return rootQuestionsWithRiskFactors;
    }


    private Question getQuestionByCode(String questionCode){
        for (Question question: this.questions){
            if (question.getQuestionCode().equals(questionCode)){
                return question;
            }
        }
        return null;
    }


    private void setPossibleAnswersForQuestions() {
         for (Question question: this.questions){
            if (null!=question.getAnswerGroupCode() && !question.getAnswerGroupCode().equals("Free Text") ){
                List<Answer> possibleAnswers = new ArrayList<Answer>();
                for (Answer answer: this.answers){
                    if (answer.getAnswerGroupCode().equals(question.getAnswerGroupCode())){
                        possibleAnswers.add(answer);
                    }
                }
                question.setPossibleAnswers(possibleAnswers);
            }
        }
    }

    private void setDefaultAnswersForQuestions(){
        for (Question question: this.questions){
            if (question.getDefaultAnswerCode()!=null){
                 for (Answer answer: this.answers){
                    if (answer.getAnswerCode().equals(question.getDefaultAnswerCode())){
                        question.setDefaultAnswer(answer);
                        return;
                    }
                }
            }
        }
    }

    private void buildRiskFactorsFromQuestions(List<Question> questions, RiskFactorValue parentRiskFactorValue, boolean isRoot){
        for(Question question : questions){

            RiskFactor riskFactor = new RiskFactor(//each question is guaranteed to have a risk factor by the specialized calls to QA Model to only pull specific questions
                    question.getQuestionCode(),
                    question.getRiskFactorCode());

            if(null!=question.getAnswerFormatType() && "Free Text"!=question.getAnswerFormatType()) {
                for (Answer answer : question.getPossibleAnswersWithRiskFactorValues()) {

                    RiskFactorValue riskFactorValue = new RiskFactorValue(
                            answer.getAnswerCode(),
                            getScoreFor(question.getRiskFactorCode(), answer.getRiskFactorValueCode()),
                            riskFactor);
                    //they API call for child questions accepts more than one answer, so we need to pass our answer as a list
                    List<String> answerCodes = new ArrayList<String>();
                    answerCodes.add(answer.getAnswerCode());

                    List<Question> childQuestions = this.getNextQuestionsWithRiskFactors(
                            question.getQuestionCode(),
                            answerCodes);
                    riskFactor.addRiskFactorValue(riskFactorValue);
                    buildRiskFactorsFromQuestions(childQuestions, riskFactorValue, false);

                }
            }
            if (isRoot){
                    this.rootRiskFactors.add(riskFactor);
            }
            else{
                parentRiskFactorValue.addNextRiskFactor(riskFactor);
            }

        }
    }


    private void setNextQuestionRulesMap(){

        this.nextQuestionsRulesMap = new HashMap<String,List<Question>>();
        List questionList=null;
        for (NextQuestionRule rule: this.nextQuestionRules){
            Question question = getQuestionByCode(rule.getNextQuestionCode());
            //this.nextQuestionsRulesMap.put(getQAKeyBasedOnCodes(rule.getQuestionCode(),rule.getAnswerCode()),question);//todo add collection of quesions
            String rulesMapKey=getQAKeyBasedOnCodes(rule.getQuestionCode(),rule.getAnswerCode());
            if(!this.nextQuestionsRulesMap.containsKey(rulesMapKey))
            {
                questionList=new ArrayList<Question>();
                questionList.add(question);
                this.nextQuestionsRulesMap.put(rulesMapKey,questionList);
            }
            else
            {
                List<Question> questionValueList=this.nextQuestionsRulesMap.get(rulesMapKey);
                questionValueList.add(question);
                this.nextQuestionsRulesMap.replace(rulesMapKey,this.nextQuestionsRulesMap.get(rulesMapKey),questionValueList);
            }
        }
    }
    public Set<String> convertQuestionCodetoRiskFactorCode(Set<String> kycFacts)
    {
        Set riskFactorCodeSet=new HashSet();
        for (String fact : kycFacts) {
            String questionCode= StringUtils.substringBefore(fact,":");
            String factID=getRiskFactorCodeValue(questionCode);
            String answerCode=StringUtils.substringAfter(fact,":");
            if( null!=factID){
                    riskFactorCodeSet.add(factID.concat(":").concat(answerCode));
                }
            else
            {
                riskFactorCodeSet.add(fact);
            }

            }
        return riskFactorCodeSet;
    }
    //Method to convert the riskFactor to questionCode(tag) for
    //display
    public Set<String> convertRiskFactorCodeToQuestionCode(Set<String> facts,boolean isSkipped)
    {
        Set questionCodeSet=new HashSet();
        if (!isSkipped) {
        for (String fact : facts) {
            String question="";
            String answer="";
            if(fact.contains(":")){
                question= StringUtils.substringBefore(fact, ":");
                answer=StringUtils.substringAfter(fact, ":");
            }
            else{question=fact;}
            String questionCodeForRiskFact=getQuestionCodeValue(question);
            if(""!=questionCodeForRiskFact){
                if(""!=answer){
                questionCodeSet.add(questionCodeForRiskFact.concat(":").concat(answer));}
                else
                {
                    questionCodeSet.add(questionCodeForRiskFact);
                }
            }
            else{
                questionCodeSet.add(fact);
            }
        }return questionCodeSet;
        }else{
            Set questionCodeSkippedSet=new HashSet();
            for (String fact : facts)
            {
                String questionCodeForRiskFact = getQuestionCodeValue(StringUtils.substringBefore(fact, ":"));
                String answer = StringUtils.substringAfter(fact, ":");
                if ("" != questionCodeForRiskFact) {
                    questionCodeSkippedSet.add(questionCodeForRiskFact.concat(":").concat(answer));
                } else {
                    questionCodeSkippedSet.add(fact);
                }
            }
            return questionCodeSkippedSet;
            }
    }

    private String getRiskFactorCodeValue(String questionCode)
    {
         for (Question question : this.questions){
            if (questionCode.equals(question.getQuestionCode())){
                  return question.getRiskFactorCode();
            }
        }
        return "";
    }
    private String getQuestionCodeValue(String riskFactorCode)
    {
        for (Question question : this.questions){
            if (riskFactorCode.equals(question.getRiskFactorCode())){
                return question.getQuestionCode();
            }
        }
        return "";
    }



}
