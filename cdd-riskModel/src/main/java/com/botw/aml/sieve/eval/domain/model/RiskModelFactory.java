package com.botw.aml.sieve.eval.domain.model;

import com.botw.aml.sieve.eval.persistence.AnswerRepository;
import com.botw.aml.sieve.eval.persistence.NextQuestionRuleRepository;
import com.botw.aml.sieve.eval.persistence.QuestionRepository;
import com.botw.aml.sieve.eval.persistence.RiskScoreRuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by marute01 on 03/04/2016.
 */
@Component
public class RiskModelFactory {



    @Autowired
    private NextQuestionRuleRepository nextQuestionRuleRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private RiskScoreRuleRepository riskScoreRuleRepository;


    public RiskModelFactory(){

    }


    public RiskModel createRiskModel(){

        RiskModel riskModel = new RiskModel(
                questionRepository.findAll(),
                answerRepository.findAll(),
                riskScoreRuleRepository.findAll(),
                nextQuestionRuleRepository.findAll(),
                "IRIS", "v.1");


        return riskModel;

    }



}
