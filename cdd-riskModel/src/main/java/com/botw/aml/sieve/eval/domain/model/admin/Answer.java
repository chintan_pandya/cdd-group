package com.botw.aml.sieve.eval.domain.model.admin;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by marute01 on 03/01/2016.
 */

@Entity
public class Answer implements Serializable{

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY) long id;
    @Getter @Setter private String answerCode;
    @Getter @Setter private String answerText;

    @Getter @Setter private String answerGroupCode;
    @Getter @Setter private String riskFactorValueCode; //used it to map QA Model to Risk Model
    @Getter @Setter private String irisValue;


}


