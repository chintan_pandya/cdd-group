package com.botw.aml.sieve.eval.domain.model.admin;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by marute01 on 03/01/2016.
 */
@Entity
public class Question implements Serializable {

//NOTE: using Lombok library to eliminate getters / setters and other boilerplate

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY) long id;
    @Getter @Setter private String questionCode;
    @Getter @Setter private String questionText;
    @Getter @Setter private Boolean isRoot;
    @Getter @Setter private Boolean isDerived; //not asked directly, should be excluded from UI
    @Getter @Setter private Boolean isMultiSelect;
    @Getter @Setter private Boolean isGrid;

    @Getter @Setter private String answerFormatType; //refactor to ENUM
    @Getter @Setter private String answerGroupCode;
    @Getter @Setter private String riskFactorCode;  //used it to map QA Model to Risk Model
    @Getter @Setter private String defaultAnswerCode;


    @Transient @Getter @Setter private List<Answer> possibleAnswers;
    @Transient @Getter @Setter private Answer defaultAnswer;

    public List<Answer> getPossibleAnswersWithRiskFactorValues(){

        List<Answer> riskFactorAnswers = new ArrayList<Answer>();
        if(null!=possibleAnswers) {
            for (Answer answer : possibleAnswers) {
                if (null != answer.getRiskFactorValueCode()) {
                    riskFactorAnswers.add(answer);
                }
            }
        }
        return riskFactorAnswers;
    }

}
