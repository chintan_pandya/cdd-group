package com.botw.aml.sieve.eval.persistence;

import com.botw.aml.sieve.eval.domain.model.admin.Question;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by marute01 on 03/01/2016.
 */
public interface QuestionRepository  extends CrudRepository<Question, String> {
}