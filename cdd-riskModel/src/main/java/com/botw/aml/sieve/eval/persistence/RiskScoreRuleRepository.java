package com.botw.aml.sieve.eval.persistence;

import com.botw.aml.sieve.eval.domain.model.admin.RiskScoreRule;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by marute01 on 03/01/2016.
 */
public interface RiskScoreRuleRepository extends CrudRepository<RiskScoreRule, String> {
}